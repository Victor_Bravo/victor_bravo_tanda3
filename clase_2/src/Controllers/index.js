const { db } = require("../Models");
const { setTimeout } = require("timers/promises");
async function ExistUser({ id }) {
  try {
    const match = db.some((el) => el.id === id);
    return { statusCode: 200, data: match };
  } catch (error) {
    console.log({ step: "service Controllers", error: error.toString() });
    return { statusCode: 500, message: error.toString() };
  }

  return info;
}
async function FindUser({ id }) {
  try {
    const user = db.filter((el) => el.id === id)[0];
    console.log(user);
    await setTimeout(10000);
    return { statusCode: 200, data: user };
  } catch (error) {
    console.log({ step: "service Controllers", error: error.toString() });
    return { statusCode: 500, message: error.toString() };
  }

  return info;
}

module.exports = { FindUser, ExistUser };
