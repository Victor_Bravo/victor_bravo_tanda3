const db = [
  {
    id: 1,
    info: {
      name: "Victor",
      edad: 22,
      domicilio: "aragua",
      nacionalidad: "venezuela",
    },
    color: "azul",
  },
  {
    id: 2,
    info: {
      name: "Manuel",
      edad: 23,
      domicilio: "aragua",
      nacionalidad: "venezuela",
    },
    color: "azul",
  },
  {
    id: 3,
    info: {
      name: "Keillyn",
      edad: 27,
      domicilio: "aragua",
      nacionalidad: "venezuela",
    },
    color: "azul",
  },
];
module.exports = { db };
