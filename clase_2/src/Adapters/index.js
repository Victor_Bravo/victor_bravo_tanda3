const { servicio } = require("../Services");
async function Adaptador({ id }) {
  try {
    let { statusCode, data, message } = await servicio({ id });
    return { statusCode, data, message };
  } catch (error) {
    console.log({ step: "service Adaptador", error: error.toString() });
    return { statusCode: 500, message: error.toString() };
  }
}

module.exports = { Adaptador };
