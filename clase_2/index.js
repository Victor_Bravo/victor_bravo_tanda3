const { Adaptador } = require("./src/Adapters");
const main = async () => {
  try {
    const result = await Adaptador({ id: 1 });
    if (result.statusCode != 200) {
      throw result.message;
    } else {
      console.log("Tus datos son", result.data);
    }
  } catch (error) {
    console.log(error);
  }
};
main();
