const { personaldata, preferenciascolor } = require("../Controllers");
function servicio({ info, color }) {
  const personalData = personaldata({ info });
  const personalcolor = preferenciascolor({ color });

  return { personalData, personalcolor };
}
module.exports = { servicio };
//! el servicio puede combinar diferentes controladores
