const { servicio } = require("../Services");
function Adaptador({ info, color }) {
  const result = servicio({ info, color });
  return result;
}

module.exports = { Adaptador };
