const { Adaptador } = require("./Adapters");
const main = () => {
  try {
    const result = Adaptador({
      info: {
        name: "Manuel",
        edad: 22,
        editor: "atom",
        domicilio: "aragua",
        nacionalidad: "venezuela",
      },
      color: "azul",
    });
    if (result.statusCode != 200) {
      throw result.message;
    } else {
      console.log("Tus datos son", result.data);
    }
  } catch (error) {
    console.log(error);
  }
};
main();
