const { servicio } = require("../Services");
function Adaptador({ info, color }) {
  try {
    let { statusCode, data, message } = servicio({ info, color });
    return { statusCode, data, message };
  } catch (error) {
    console.log({ step: "service Adaptador", error: error.toString() });
    return { statusCode: 500, message: error.toString() };
  }
}

module.exports = { Adaptador };
