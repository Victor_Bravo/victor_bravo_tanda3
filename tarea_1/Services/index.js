const { PersonalData, PreferencesColor } = require("../Controllers");
function servicio({ info, color }) {
  const personaldata = PersonalData({ info });
  try {
    if (info.edad < 18) throw "Necesitas ser mayor de edad";
    const personalcolor = PreferencesColor({ color });
    return { statusCode: 200, data: { personaldata, personalcolor } };
  } catch (error) {
    console.log({ step: "service Servicio", error: error.toString() });

    return { statusCode: 500, message: error.toString() };
  }
}
module.exports = { servicio };
//! el servicio puede combinar diferentes controladores
//? 500 significa error interno
