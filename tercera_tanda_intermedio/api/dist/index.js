"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const socket_io_client_1 = require("socket.io-client");
const socket = (0, socket_io_client_1.io)("http://localhost:3000");
async function main() {
    try {
        setTimeout(() => console.log(socket.id), 1500);
        socket.on('res:user:view', ({ statusCode, data, message }) => {
            console.log('res:user:view', { statusCode, data, message });
        });
        socket.on("connect error", (err) => {
            console.log(err.message);
        });
        // socket.on('res:microservice:create', ({ statusCode, data, message }) => {
        //     console.log('res:microservice:create', { statusCode, data, message });
        // });
        // socket.on('res:microservice:findOne', ({ statusCode, data, message }) => {
        //     console.log('res:microservice:findOne', { statusCode, data, message });
        // });
        // socket.on('res:microservice:update', ({ statusCode, data, message }) => {
        //     console.log('res:microservice:update', { statusCode, data, message });
        // });
        // socket.on('res:microservice:delete', ({ statusCode, data, message }) => {
        //     console.log('res:microservice:delete', { statusCode, data, message });
        // });
        // setInterval(() => socket.emit('req:microservice:view',({})), 5000);
        setTimeout(() => {
            socket.emit('req:user:view', ({}));
        }, 300);
    }
    catch (error) {
        console.log({ statuCode: 400, message: error() });
    }
}
main();
//# sourceMappingURL=index.js.map