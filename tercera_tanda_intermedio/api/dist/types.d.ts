declare type StatusCode = 'success' | 'error' | 'notFound' | 'notPermitted' | 'ValidationError';
export declare namespace User {
    interface Paginate {
        data: Model[];
        itemCount: number;
        pageCount: number;
    }
    interface Model {
        id: number;
        fullName: string;
        username: string;
        password: string;
        state: boolean;
        image: string;
        phone: number;
        createdAt: string;
        updatedAt: string;
    }
    namespace FindOne {
        interface Request {
            username?: string;
            id?: string;
        }
        interface Response {
            statusCode: StatusCode;
            data?: Model;
            message?: string;
        }
    }
    namespace View {
        interface Request {
            offset?: number;
            limit?: number;
            state?: boolean;
        }
        interface Response {
            statusCode: StatusCode;
            data?: Paginate;
            message?: string;
        }
    }
    namespace Update {
        interface Request {
            username: string;
            fullName?: string;
            phone?: number;
            image?: string;
        }
        interface Response {
            statusCode: StatusCode;
            data?: Model;
            message?: string;
        }
    }
    namespace Create {
        interface Request {
            username: string;
            fullName: string;
            phone: number;
            image: string;
        }
        interface Response {
            statusCode: StatusCode;
            data?: Model;
            message?: string;
        }
    }
    namespace Delete {
        interface Request {
            username: string;
        }
        interface Response {
            statusCode: StatusCode;
            data?: Model;
            message?: string;
        }
    }
}
export declare namespace Product {
    interface Paginate {
        data: Model[];
        itemCount: number;
        pageCount: number;
    }
    interface Model {
        id: number;
        name: string;
        mark: string;
        year: string;
        image: string;
        state: boolean;
        bodega: string;
        lote: number;
        createdAt: string;
        updatedAt: string;
    }
    namespace FindOne {
        interface Request {
            id: string;
        }
        interface Response {
            statusCode: StatusCode;
            data?: Model;
            message?: string;
        }
    }
    namespace View {
        interface Request {
            offset?: number;
            limit?: number;
            year?: string;
            state?: boolean;
            marks?: string[];
            bodegas?: string[];
            lotes?: string[];
        }
        interface Response {
            statusCode: StatusCode;
            data?: Model;
            message?: string;
        }
    }
    namespace Update {
        interface Request {
            id: number;
            name?: string;
            mark?: string;
            year?: string;
            image?: string;
            state?: boolean;
            bodega?: string;
            lote?: number;
        }
        interface Response {
            statusCode: StatusCode;
            data?: Model;
            message?: string;
        }
    }
    namespace Create {
        interface Request {
            name: string;
            mark: string;
            year?: string;
            image?: string;
            bodega?: string;
            lote?: number;
        }
        interface Response {
            statusCode: StatusCode;
            data?: Model;
            message?: string;
        }
    }
    namespace Delete {
        interface Request {
            ids?: number[];
            marks?: string[];
            years?: string[];
            state?: boolean;
            bodegas?: string[];
            lotes?: number[];
        }
        interface Response {
            statusCode: StatusCode;
            data?: number;
            message?: string;
        }
    }
}
export declare namespace Shopp {
    interface Paginate {
        data: Model[];
        itemCount: number;
        pageCount: number;
    }
    interface Model {
        id: number;
        user: string;
        product: string;
        createdAt?: string;
        updatedAt?: string;
    }
    namespace View {
        interface Request {
            offset?: number;
            limit?: number;
            users?: string[];
            products?: string[];
        }
        interface Response {
            statusCode: StatusCode;
            data?: Paginate;
            message?: string;
        }
    }
    namespace Create {
        interface Request {
            user: string;
            product: string;
        }
        interface Response {
            statusCode: StatusCode;
            data?: Model;
            message?: string;
        }
    }
    namespace Delete {
        interface Request {
            ids?: number[];
            users?: string[];
            products?: string[];
        }
        interface Response {
            statusCode: StatusCode;
            data?: number;
            message?: string;
        }
    }
}
export {};
//# sourceMappingURL=types.d.ts.map