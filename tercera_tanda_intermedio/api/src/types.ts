type StatusCode = 'success' | 'error' | 'notFound' | 'notPermitted' | 'ValidationError';

export namespace User {

    export interface Paginate {
        data: Model[],
        itemCount: number,
        pageCount: number
    };

    export interface Model {
        id: number;

        fullName: string;

        username: string;

        password: string;

        state: boolean;

        image: string;

        phone: number;

        createdAt: string;

        updatedAt: string;
    };


    export namespace FindOne {

        export interface Request {
            username?: string;
            id?: string;
        }

        export interface Response {
            statusCode: StatusCode;
            data?: Model;
            message?: string;
        }
    }

    export namespace View {

        export interface Request {
            offset?: number;
            limit?: number;
            state?: boolean;
        }

        export interface Response {
            statusCode: StatusCode;
            data?: Paginate;
            message?: string;
        }
    }

    export namespace Update {

        export interface Request {
            username: string;
            fullName?: string;
            phone?: number;
            image?: string;
        }

        export interface Response {
            statusCode: StatusCode;
            data?: Model;
            message?: string;
        }
    }

    export namespace Create {

        export interface Request {
            username: string;
            fullName: string;
            phone: number;
            image: string;
        }

        export interface Response {
            statusCode: StatusCode;
            data?: Model;
            message?: string;
        }
    }

    export namespace Delete {

        export interface Request {
            username: string;
        }

        export interface Response {
            statusCode: StatusCode;
            data?: Model;
            message?: string;
        }
    };


};

export namespace Product {
    export interface Paginate {
        data: Model[],
        itemCount: number,
        pageCount: number
    };

    export interface Model {
        id: number;

        name: string;

        mark: string;

        year: string;

        image: string;

        state: boolean;

        bodega: string;

        lote: number;

        createdAt: string;

        updatedAt: string;
    };


    export namespace FindOne {

        export interface Request {
            id: string;
        }

        export interface Response {
            statusCode: StatusCode;
            data?: Model;
            message?: string;
        }
    }

    export namespace View {

        export interface Request {
            offset?: number;
            limit?: number;

            year?: string;
            state?: boolean;

            marks?: string[];
            bodegas?: string[];
            lotes?: string[];
        }

        export interface Response {
            statusCode: StatusCode;
            data?: Model;
            message?: string;
        }
    }

    export namespace Update {

        export interface Request {
            id: number;
            name?: string;
            mark?: string;
            year?: string;
            image?: string;
            state?: boolean;
            bodega?: string;
            lote?: number;
        }

        export interface Response {
            statusCode: StatusCode;
            data?: Model;
            message?: string;
        }
    }

    export namespace Create {

        export interface Request {
            name: string;
            mark: string;
            year?: string;
            image?: string;
            bodega?: string;
            lote?: number;
        }

        export interface Response {
            statusCode: StatusCode;
            data?: Model;
            message?: string;
        }
    }

    export namespace Delete {

        export interface Request {
            ids?: number[];
            marks?: string[];
            years?: string[];
            state?: boolean;
            bodegas?: string[];
            lotes?: number[];
        }

        export interface Response {
            statusCode: StatusCode;
            data?: number;
            message?: string;
        }
    }
};

export namespace Shopp {
    export interface Paginate {
        data: Model[],
        itemCount: number,
        pageCount: number
    };

    export interface Model {
        id: number;

        user: string;

        product: string;

        createdAt?: string;

        updatedAt?: string;
    };

    export namespace View {

        export interface Request {
            offset?: number;
            limit?: number;

            users?: string[];
            products?: string[];
        }

        export interface Response {
            statusCode: StatusCode;
            data?: Paginate;
            message?: string;
        }
    }

    export namespace Create {

        export interface Request {
            user: string;
            product: string;
        }

        export interface Response {
            statusCode: StatusCode;
            data?: Model;
            message?: string;
        }
    }

    export namespace Delete {

        export interface Request {
            ids?: number[];
            users?: string[];
            products?: string[];
        }

        export interface Response {
            statusCode: StatusCode;
            data?: number;
            message?: string;
        }
    }
};