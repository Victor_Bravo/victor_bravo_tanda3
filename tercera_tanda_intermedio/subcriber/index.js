const dotenv = require('dotenv');
const { createClient } = require ('redis');

dotenv.config();

const REDIS = {
    host: process.env.REDIS_HOST,
    port: parseInt(process.env.REDIS_PORT),
    password: process.env.REDIS_PASS
};

const redisClient = createClient({
    url: `redis://${REDIS.host}:${REDIS.port}`,
    password: REDIS.password
});

async function main() {
    try {
        redisClient.on('error', (err) => console.log('Redis Client Error', err));

        await redisClient.connect();

        await redisClient.pSubscribe('*', async(message, channel)=>console.log({message, channel}));

    } catch (error) {
        console.error();
    }
}

main();