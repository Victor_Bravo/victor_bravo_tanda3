const api = require('../dist');

const redis ={
    local:{
        host:"localhost",
        port:6379,
        password:undefined
    }
}

const environmet = redis.local;

const users = [
{
    fullName:"Victor Bravo",
    image:"victor.jpg",
    phone:2912098309,
    username:"VB1999"
},
{
    fullName:"Victor Bravo",
    image:"victor.jpg",
    phone:2912098309,
    username:"VB1999"
}
];

async function create(user) {
    try {

        const result = await api.Create(user, environmet);

        console.log(result);
        
    } catch (error) { console.error(error) }
    
};

async function del(username) {
    try {

        const result = await api.Delete({username}, environmet);

        console.log(result);
        
    } catch (error) { console.error(error) }
};

async function update(params) {
    try {

        const result = await api.Update(params, environmet);

        console.log(result);
        
    } catch (error) { console.error(error) }
};

async function findOne(username) {
    try {

        const result = await api.FindOne({username}, environmet);

        console.log(result);
        
    } catch (error) { console.error(error) }
};

async function view(params) {
    try {

        const result = await api.View(params, environmet);

        console.log(result);
        
    } catch (error) { console.error(error) }
};

const main = async ()=>{
    try {

        await view();

        await create(users[0]);

        await view();

        // await del(users[0].username);

    } catch (error) { console.error(error) }
};

main();
