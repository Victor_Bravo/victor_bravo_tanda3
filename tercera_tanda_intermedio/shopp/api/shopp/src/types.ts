type StatusCode = 'success' | 'error' | 'notFound' | 'notPermitted' | 'ValidationError';

export const endpoint = ['create', 'password', 'delete', 'findOne', 'view', 'update'] as const;

export type Endpoint = typeof endpoint[number];

export interface Paginate {
    data: Model[],
    itemCount: number,
    pageCount: number
};

export interface Model {
    id?: number;

    user: string;

    product: string;

    createdAt?: string;

    updatedAt?: string;
};

export namespace View {

    export interface Request {
        offset?: number;
        limit?: number;

        users?: string[];
        products?: string[];
    }

    export interface Response {
        statusCode: StatusCode;
        data?: Paginate;
        message?: string;
    }
}

export namespace Create {

    export interface Request {
        user: string;
        product: string;
    }

    export interface Response {
        statusCode: StatusCode;
        data?: Model;
        message?: string;
    }
}

export namespace Delete {

    export interface Request {
        ids?: number[];
        users?: string[];
        products?: string[];
    }

    export interface Response {
        statusCode: StatusCode;
        data?: number;
        message?: string;
    }
}



export interface REDIS {

    host: string;
    port: number;
    password: string;

};


