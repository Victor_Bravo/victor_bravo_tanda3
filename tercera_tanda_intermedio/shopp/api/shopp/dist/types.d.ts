declare type StatusCode = 'success' | 'error' | 'notFound' | 'notPermitted' | 'ValidationError';
export declare const endpoint: readonly ["create", "password", "delete", "findOne", "view", "update"];
export declare type Endpoint = typeof endpoint[number];
export interface Paginate {
    data: Model[];
    itemCount: number;
    pageCount: number;
}
export interface Model {
    id?: number;
    user: string;
    product: string;
    createdAt?: string;
    updatedAt?: string;
}
export declare namespace View {
    interface Request {
        offset?: number;
        limit?: number;
        users?: string[];
        products?: string[];
    }
    interface Response {
        statusCode: StatusCode;
        data?: Paginate;
        message?: string;
    }
}
export declare namespace Create {
    interface Request {
        user: string;
        product: string;
    }
    interface Response {
        statusCode: StatusCode;
        data?: Model;
        message?: string;
    }
}
export declare namespace Delete {
    interface Request {
        ids?: number[];
        users?: string[];
        products?: string[];
    }
    interface Response {
        statusCode: StatusCode;
        data?: number;
        message?: string;
    }
}
export interface REDIS {
    host: string;
    port: number;
    password: string;
}
export {};
//# sourceMappingURL=types.d.ts.map