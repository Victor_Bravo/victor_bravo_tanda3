"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.view = exports.del = exports.create = void 0;
const joi_1 = __importDefault(require("joi"));
async function create(params) {
    try {
        const shcema = joi_1.default.object({
            user: joi_1.default.number().required(),
            product: joi_1.default.number().required(),
        });
        const result = await shcema.validateAsync(params);
        return { statusCode: 'success', data: params };
    }
    catch (error) {
        throw { statusCode: "error", message: error.toString() };
    }
}
exports.create = create;
async function del(params) {
    try {
        const shcema = joi_1.default.object({
            ids: joi_1.default.array().items(joi_1.default.number().required()),
            users: joi_1.default.array().items(joi_1.default.string().required()),
            products: joi_1.default.array().items(joi_1.default.string().required()),
        }).xor('ids', 'users', 'products');
        const result = await shcema.validateAsync(params);
        return { statusCode: 'success', data: params };
    }
    catch (error) {
        throw { statusCode: "error", message: error.toString() };
    }
}
exports.del = del;
async function view(params) {
    try {
        const shcema = joi_1.default.object({
            offset: joi_1.default.number(),
            limit: joi_1.default.number(),
            users: joi_1.default.array().items(joi_1.default.string().required()),
            products: joi_1.default.array().items(joi_1.default.string().required()),
        });
        const result = await shcema.validateAsync(params);
        return { statusCode: "success", data: params };
    }
    catch (error) {
        throw { statusCode: "error", message: error.toString() };
    }
}
exports.view = view;
//# sourceMappingURL=index.js.map