declare type StatusCode = 'success' | 'error' | 'notFound' | 'notPermitted' | 'ValidationError';
export declare namespace View {
    interface Request {
        offset?: number;
        limit?: number;
        users?: string[];
        products?: string[];
    }
    interface Response {
        statusCode: StatusCode;
        data?: Request;
        message?: string;
    }
}
export declare namespace Create {
    interface Request {
        user: string;
        product: string;
    }
    interface Response {
        statusCode: StatusCode;
        data?: Request;
        message?: string;
    }
}
export declare namespace Delete {
    interface Request {
        ids?: number[];
        users?: string[];
        products?: string[];
    }
    interface Response {
        statusCode: StatusCode;
        data?: Request;
        message?: string;
    }
}
export {};
//# sourceMappingURL=types.d.ts.map