import { T as apiUser } from 'api-users';
import { T as apiProduct } from 'api-product';
import * as S from 'sequelize';



type StatusCode = 'success' | 'error' | 'notFound' | 'notPermitted' | 'ValidationError';

export namespace Models {

    export interface ModelsAttributes {
        id?: number;

        user?: string;

        product?: string;

        createdAt?: string;

        updatedAt?: string;
    }

    export const attributes = ['id', 'user', 'product', 'createdAt', 'updatedAt'] as const;

    export type Attributes = typeof attributes[number];

    export type where = S.WhereOptions<ModelsAttributes>

    export interface Model extends S.Model<ModelsAttributes> {

    }

    export interface Paginate {
        data: ModelsAttributes[],
        itemCount: number,
        pageCount: number
    }

    export namespace Count {

        export interface Request extends Omit<S.CountOptions<ModelsAttributes>, "group"> {

        }

        export interface Response {

            statusCode: StatusCode;
            data?: number;
            message?: string;
        }
    }

    export namespace Create {

        export interface Request extends ModelsAttributes {

        }

        export interface Opts extends S.CreateOptions<ModelsAttributes> {

        }

        export interface Response {

            statusCode: StatusCode;
            data?: ModelsAttributes;
            message?: string;
        }

    }

    export namespace Del {

        export interface Opts extends S.DestroyOptions<ModelsAttributes> {

        }

        export interface Response {

            statusCode: StatusCode;
            data?: number;
            message?: string;
        }
    }
    export namespace FindAndCountAll {

        export interface Opts extends Omit<S.FindAndCountOptions<ModelsAttributes>, "group"> {

        }

        export interface Response {

            statusCode: StatusCode;
            data?: Paginate;
            message?: string;
        }

    }

    export namespace FindOne {

        export interface Opts extends S.FindOptions<ModelsAttributes> {

        }

        export interface Response {

            statusCode: StatusCode;
            data?: ModelsAttributes;
            message?: string;
        }

    }

    export namespace Update {

        export interface Request extends ModelsAttributes {

        }

        export interface Opts extends S.UpdateOptions<ModelsAttributes> {

        }

        export interface Response {

            statusCode: StatusCode;
            data?: [number, ModelsAttributes[]];
            message?: string;
        }

    }

    export namespace SyncDB {

        export interface Request extends S.SyncOptions {

        }

        export interface Response {

            statusCode: StatusCode;
            data?: string;
            message?: string;

        }

    }

}

export namespace Services {

    export namespace View {

        export interface Request {
            offset?: number;
            limit?: number;

            users?: string[];
            products?: string[];
        }

        export interface Response {
            statusCode: StatusCode;
            data?: Models.Paginate;
            message?: string;
        }
    }

    export namespace Create {

        export interface Request {
            user: string;
            product: string;
        }

        export interface Response {
            statusCode: StatusCode;
            data?: Models.ModelsAttributes;
            message?: string;
        }
    }

    export namespace Delete {

        export interface Request {
            ids?: number[];
            users?: string[];
            products?: string[];
        }

        export interface Response {
            statusCode: StatusCode;
            data?: number;
            message?: string;
        }
    }
}

export namespace Controller {

    export namespace ValidateUser {

        export interface Request {
            user: string;
        }

        export interface Response {
            statusCode: StatusCode;
            data?: apiUser.Model;
            message?: string;
        }
    }

    export namespace ValidateProduct {

        export interface Request {
            product: string;
        }

        export interface Response {
            statusCode: StatusCode;
            data?: apiProduct.Model;
            message?: string;
        }
    }

    export namespace Publish {

        export interface Request {
            channel: string;
            instance: string;
        }

        export interface Response {
            statusCode: StatusCode;
            data?: Request;
            message?: string;
        }
    }

}

export namespace Adapter {

    export const endpoint = ['create', 'delete', 'view'] as const;

    export type Endpoint = typeof endpoint[number];

    export namespace BullConn {
        export interface opts {
            concurrency: number;
            redis: Settings.REDIS;
        }
    }



}

export namespace Settings {

    export interface REDIS {

        host: string;
        port: number;
        password: string;

    }
}
