import { name, RedisOptsQueue as opts, Action } from "../settings";
import * as Services from '../services';
import { Publish } from "../controller";
import { Worker, Job } from 'bullmq'
import { Adapter } from "../types";

export const Process = async (job: Job<any, any, Adapter.Endpoint>) => {
    try {

        switch (job.name) {

            case 'create': {

                let { statusCode, data, message } = await Services.create(job.data);

                return { statusCode, data, message };
            }

            case 'delete': {

                let { statusCode, data, message } = await Services.del(job.data);

                return { statusCode, data, message };
            }

            case 'view': {

                let { statusCode, data, message } = await Services.view(job.data);

                return { statusCode, data, message };
            }

            default: return { statusCode: 'error', message: "Method not found" }

        };

    } catch (error) {

        await Publish({ channel: Action.error, instance: JSON.stringify({ step: 'Adaptador Process', message: error }) })

    }
};


export const run = async () => {
    try {

        await Publish({ channel: Action.start, instance: JSON.stringify({ step: 'Adaptador Run', message: `starting ${name}` }) });

        const worker = new Worker(`${name}:2`, Process, { connection: opts.redis, concurrency: opts.concurrency });

        worker.on('error', async error => {
            await Publish({ channel: Action.error, instance: JSON.stringify({ step: 'Adapters run', error }) })
        })

    } catch (error) {

        await Publish({ channel: Action.error, instance: JSON.stringify({ step: 'Adaptador run', message: error }) })

    }
};