import { InternalError, redisClient, RedisOptsQueue as opts } from '../settings';
import { Controller as T } from '../types';
import * as ApiUser from 'api-users';
import * as ApiProduct from 'api-product'

export const Publish = async (props: T.Publish.Request): Promise<T.Publish.Response> => {
    try {

        if (!redisClient.isOpen) await redisClient.connect();

        await redisClient.publish(props.channel, props.instance);

        return { statusCode: "success", data: props };

    } catch (error) {

        console.error('error', { step: 'controller Publish', error });

        return { statusCode: "error", message: InternalError }

    }

}

export const ValidateUser = async (props: T.ValidateUser.Request): Promise<T.ValidateUser.Response> => {
    try {

        const { statusCode, data, message } = await ApiUser.FindOne({ id: props.user }, opts.redis)

        if (statusCode !== 'success') {

            switch (statusCode) {
                case 'notFound': throw { statusCode: "ValidationError", message: "No existe el usuario a validar" };

                default: throw { statusCode, message }
            }
        }

        return { statusCode: 'success', data }

    } catch (error) {

        console.error('error', { step: 'controller ValidateUser', error });

        return { statusCode: "error", message: InternalError }

    }

}

export const ValidateProduct = async (props: T.ValidateProduct.Request): Promise<T.ValidateProduct.Response> => {
    try {
        const { statusCode, data, message } = await ApiProduct.FindOne({ id: props.product }, opts.redis)

        if (statusCode !== 'success') {

            switch (statusCode) {
                case 'notFound': throw { statusCode: "ValidationError", message: "No existe el producto a validar" };

                default: throw { statusCode, message }
            }
        }

        return { statusCode: 'success', data }
    } catch (error) {

        console.error('error', { step: 'controller ValidateProduct', error });

        return { statusCode: "error", message: InternalError }

    }

}
