import * as Models from "../models";
import { InternalError } from "../settings";
import * as T from "../types";
import * as Validation from 'validate-shopp';
import * as Controller from '../controller';

export async function create(params: T.Services.Create.Request): Promise<T.Services.Create.Response> {

    try {

        await Validation.create(params);

        //* validar el usuario

        const user = (await Controller.ValidateUser({ user: params.user })).data;

        if (!user.state) {

            throw { statusCode: "ValidationError", message: "El usuario no esta habilitado para realizar compras" };

        };

        //* validar el producto

        const product = (await Controller.ValidateProduct({ product: params.product })).data;

        if (!product.state) {

            throw { statusCode: "ValidationError", message: "el product no hay stock para realizar ventas" };

        };

        const { statusCode, data, message } = await Models.create(params);

        return { statusCode, data, message };

    } catch (error) {

        console.error({ step: "Service Create", error: error.toString() });

        return { statusCode: "error", message: InternalError }

    }

}

export async function del(params: T.Services.Delete.Request): Promise<T.Services.Delete.Response> {

    try {

        await Validation.del(params);

        var where: T.Models.where = {}

        var optionals: T.Models.Attributes[] = ['id', 'user', 'product'];

        for (let x of optionals.map(x => x.concat('s'))) if (params[x] !== undefined) where[x.slice(0, -1)] = params[x];

        const { statusCode, data, message } = await Models.del({ where });

        if (statusCode !== 'success') return { statusCode, message };
        return { statusCode: 'success', data: data }


    } catch (error) {

        console.error({ step: "Service Delete", error: error.toString() });

        return { statusCode: "error", message: InternalError }

    }
}

export async function view(params: T.Services.View.Request): Promise<T.Services.View.Response> {

    try {

        await Validation.view(params);

        var where: T.Models.where = {}

        var optionals: T.Models.Attributes[] = ['user', 'product'];

        for (let x of optionals.map(x => x.concat('s'))) if (params[x] !== undefined) where[x.slice(0, -1)] = params[x];



        const { statusCode, data, message } = await Models.findAndCountAll({ where });

        return { statusCode, data, message };

    } catch (error) {

        console.error({ step: "Service Delete", error: error.toString() });

        return { statusCode: "error", message: InternalError };

    }
}