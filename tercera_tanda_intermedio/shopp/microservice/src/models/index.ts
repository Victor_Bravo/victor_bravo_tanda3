import * as S from 'sequelize';
import { sequelize, name } from '../settings'
import { Models as T } from '../types';


export const Model = sequelize.define<T.Model>(name, {
    user: { type: S.DataTypes.STRING },
    product: { type: S.DataTypes.STRING },

}, { freezeTableName: true });

export async function count(options?: T.Count.Request): Promise<T.Count.Response> {
    try {
        const instance: number = await Model.count(options);

        return { statusCode: 'success', data: instance };

    } catch (error) {

        console.log({ step: "Models Count", error: error.toString() });

        return { statusCode: "error", message: error.toString() };

    }
};

export async function create(values: T.Create.Request, options?: T.Create.Opts): Promise<T.Create.Response> {

    try {
        const instance: T.Model = await Model.create(values, options);

        return { statusCode: 'success', data: instance.toJSON() };

    } catch (error) {

        console.log({ step: "Models Create", error: error.toString() });

        return { statusCode: "error", message: error.toString() };

    }

};

export async function del(options?: T.Del.Opts): Promise<T.Del.Response> {

    try {

        const instance: number = await Model.destroy(options)

        return { statusCode: 'success', data: instance };



    } catch (error) {

        console.log({ step: "Models Del", error: error.toString() });

        return { statusCode: "error", message: error.toString() };

    }

};

export async function findAndCountAll(options?: T.FindAndCountAll.Opts): Promise<T.FindAndCountAll.Response> {

    try {

        var options: T.FindAndCountAll.Opts = { ...{ limit: 12, offset: 0 }, ...options }

        const { count, rows }: {
            rows: T.Model[];
            count: number;
        } = await Model.findAndCountAll(options);

        return {
            statusCode: 'success', data: { data: rows.map(x => x.toJSON()), itemCount: count, pageCount: Math.ceil(count / options.limit) }
        }


    } catch (error) {

        console.log({ step: "Models View", error: error.toString() });

        return { statusCode: "error", message: error.toString() };

    }

};

export async function findOne(options?: T.FindOne.Opts): Promise<T.FindOne.Response> {

    try {
        const instance: T.Model | null = await Model.findOne(options)
        if (instance) return { statusCode: 'success', data: instance.toJSON() }

        else return { statusCode: 'notFound', message: "not Found" }

    } catch (error) {

        console.log({ step: "Models FindOne", error: error.toString() });

        return { statusCode: "error", message: error.toString() };

    }

};

export async function update(values: T.Update.Request, options?: T.Update.Opts): Promise<T.Update.Response> {

    try {

        var options: T.Update.Opts = { ...{ returning: true }, ...options }
        const instances: [number, T.Model[]] = await Model.update(values, options);


        return { statusCode: 'success', data: [instances[0], instances[1].map(x => x.toJSON())] }


    } catch (error) {

        console.log({ step: "Models update", error: error.toString() });

        return { statusCode: "error", message: error.toString() };

    }

};

export async function SyncDB(params: T.SyncDB.Request): Promise<T.SyncDB.Response> {
    try {

        const model = await Model.sync(params);

        return { statusCode: 'success', data: model.toString() };

    } catch (error) {

        console.log({ step: "Models SymcDb", error: error.toString() });

        return { statusCode: "error", message: error.toString() };
    }
    ;
}