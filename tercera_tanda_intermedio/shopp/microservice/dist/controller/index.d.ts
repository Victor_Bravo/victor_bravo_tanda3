import { Controller as T } from '../types';
export declare const Publish: (props: T.Publish.Request) => Promise<T.Publish.Response>;
export declare const ValidateUser: (props: T.ValidateUser.Request) => Promise<T.ValidateUser.Response>;
export declare const ValidateProduct: (props: T.ValidateProduct.Request) => Promise<T.ValidateProduct.Response>;
//# sourceMappingURL=index.d.ts.map