"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ValidateProduct = exports.ValidateUser = exports.Publish = void 0;
const settings_1 = require("../settings");
const ApiUser = __importStar(require("api-users"));
const ApiProduct = __importStar(require("api-product"));
const Publish = async (props) => {
    try {
        if (!settings_1.redisClient.isOpen)
            await settings_1.redisClient.connect();
        await settings_1.redisClient.publish(props.channel, props.instance);
        return { statusCode: "success", data: props };
    }
    catch (error) {
        console.error('error', { step: 'controller Publish', error });
        return { statusCode: "error", message: settings_1.InternalError };
    }
};
exports.Publish = Publish;
const ValidateUser = async (props) => {
    try {
        const { statusCode, data, message } = await ApiUser.FindOne({ id: props.user }, settings_1.RedisOptsQueue.redis);
        if (statusCode !== 'success') {
            switch (statusCode) {
                case 'notFound': throw { statusCode: "ValidationError", message: "No existe el usuario a validar" };
                default: throw { statusCode, message };
            }
        }
        return { statusCode: 'success', data };
    }
    catch (error) {
        console.error('error', { step: 'controller ValidateUser', error });
        return { statusCode: "error", message: settings_1.InternalError };
    }
};
exports.ValidateUser = ValidateUser;
const ValidateProduct = async (props) => {
    try {
        const { statusCode, data, message } = await ApiProduct.FindOne({ id: props.product }, settings_1.RedisOptsQueue.redis);
        if (statusCode !== 'success') {
            switch (statusCode) {
                case 'notFound': throw { statusCode: "ValidationError", message: "No existe el producto a validar" };
                default: throw { statusCode, message };
            }
        }
        return { statusCode: 'success', data };
    }
    catch (error) {
        console.error('error', { step: 'controller ValidateProduct', error });
        return { statusCode: "error", message: settings_1.InternalError };
    }
};
exports.ValidateProduct = ValidateProduct;
//# sourceMappingURL=index.js.map