import { T as apiUser } from 'api-users';
import { T as apiProduct } from 'api-product';
import * as S from 'sequelize';
declare type StatusCode = 'success' | 'error' | 'notFound' | 'notPermitted' | 'ValidationError';
export declare namespace Models {
    interface ModelsAttributes {
        id?: number;
        user?: string;
        product?: string;
        createdAt?: string;
        updatedAt?: string;
    }
    const attributes: readonly ["id", "user", "product", "createdAt", "updatedAt"];
    type Attributes = typeof attributes[number];
    type where = S.WhereOptions<ModelsAttributes>;
    interface Model extends S.Model<ModelsAttributes> {
    }
    interface Paginate {
        data: ModelsAttributes[];
        itemCount: number;
        pageCount: number;
    }
    namespace Count {
        interface Request extends Omit<S.CountOptions<ModelsAttributes>, "group"> {
        }
        interface Response {
            statusCode: StatusCode;
            data?: number;
            message?: string;
        }
    }
    namespace Create {
        interface Request extends ModelsAttributes {
        }
        interface Opts extends S.CreateOptions<ModelsAttributes> {
        }
        interface Response {
            statusCode: StatusCode;
            data?: ModelsAttributes;
            message?: string;
        }
    }
    namespace Del {
        interface Opts extends S.DestroyOptions<ModelsAttributes> {
        }
        interface Response {
            statusCode: StatusCode;
            data?: number;
            message?: string;
        }
    }
    namespace FindAndCountAll {
        interface Opts extends Omit<S.FindAndCountOptions<ModelsAttributes>, "group"> {
        }
        interface Response {
            statusCode: StatusCode;
            data?: Paginate;
            message?: string;
        }
    }
    namespace FindOne {
        interface Opts extends S.FindOptions<ModelsAttributes> {
        }
        interface Response {
            statusCode: StatusCode;
            data?: ModelsAttributes;
            message?: string;
        }
    }
    namespace Update {
        interface Request extends ModelsAttributes {
        }
        interface Opts extends S.UpdateOptions<ModelsAttributes> {
        }
        interface Response {
            statusCode: StatusCode;
            data?: [number, ModelsAttributes[]];
            message?: string;
        }
    }
    namespace SyncDB {
        interface Request extends S.SyncOptions {
        }
        interface Response {
            statusCode: StatusCode;
            data?: string;
            message?: string;
        }
    }
}
export declare namespace Services {
    namespace View {
        interface Request {
            offset?: number;
            limit?: number;
            users?: string[];
            products?: string[];
        }
        interface Response {
            statusCode: StatusCode;
            data?: Models.Paginate;
            message?: string;
        }
    }
    namespace Create {
        interface Request {
            user: string;
            product: string;
        }
        interface Response {
            statusCode: StatusCode;
            data?: Models.ModelsAttributes;
            message?: string;
        }
    }
    namespace Delete {
        interface Request {
            ids?: number[];
            users?: string[];
            products?: string[];
        }
        interface Response {
            statusCode: StatusCode;
            data?: number;
            message?: string;
        }
    }
}
export declare namespace Controller {
    namespace ValidateUser {
        interface Request {
            user: string;
        }
        interface Response {
            statusCode: StatusCode;
            data?: apiUser.Model;
            message?: string;
        }
    }
    namespace ValidateProduct {
        interface Request {
            product: string;
        }
        interface Response {
            statusCode: StatusCode;
            data?: apiProduct.Model;
            message?: string;
        }
    }
    namespace Publish {
        interface Request {
            channel: string;
            instance: string;
        }
        interface Response {
            statusCode: StatusCode;
            data?: Request;
            message?: string;
        }
    }
}
export declare namespace Adapter {
    const endpoint: readonly ["create", "delete", "view"];
    type Endpoint = typeof endpoint[number];
    namespace BullConn {
        interface opts {
            concurrency: number;
            redis: Settings.REDIS;
        }
    }
}
export declare namespace Settings {
    interface REDIS {
        host: string;
        port: number;
        password: string;
    }
}
export {};
//# sourceMappingURL=types.d.ts.map