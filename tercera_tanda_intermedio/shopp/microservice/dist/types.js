"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Adapter = exports.Models = void 0;
var Models;
(function (Models) {
    Models.attributes = ['id', 'user', 'product', 'createdAt', 'updatedAt'];
})(Models = exports.Models || (exports.Models = {}));
var Adapter;
(function (Adapter) {
    Adapter.endpoint = ['create', 'delete', 'view'];
})(Adapter = exports.Adapter || (exports.Adapter = {}));
//# sourceMappingURL=types.js.map