"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SyncDB = exports.update = exports.findOne = exports.findAndCountAll = exports.del = exports.create = exports.count = exports.Model = void 0;
const S = __importStar(require("sequelize"));
const settings_1 = require("../settings");
exports.Model = settings_1.sequelize.define(settings_1.name, {
    user: { type: S.DataTypes.STRING },
    product: { type: S.DataTypes.STRING },
}, { freezeTableName: true });
async function count(options) {
    try {
        const instance = await exports.Model.count(options);
        return { statusCode: 'success', data: instance };
    }
    catch (error) {
        console.log({ step: "Models Count", error: error.toString() });
        return { statusCode: "error", message: error.toString() };
    }
}
exports.count = count;
;
async function create(values, options) {
    try {
        const instance = await exports.Model.create(values, options);
        return { statusCode: 'success', data: instance.toJSON() };
    }
    catch (error) {
        console.log({ step: "Models Create", error: error.toString() });
        return { statusCode: "error", message: error.toString() };
    }
}
exports.create = create;
;
async function del(options) {
    try {
        const instance = await exports.Model.destroy(options);
        return { statusCode: 'success', data: instance };
    }
    catch (error) {
        console.log({ step: "Models Del", error: error.toString() });
        return { statusCode: "error", message: error.toString() };
    }
}
exports.del = del;
;
async function findAndCountAll(options) {
    try {
        var options = { ...{ limit: 12, offset: 0 }, ...options };
        const { count, rows } = await exports.Model.findAndCountAll(options);
        return {
            statusCode: 'success', data: { data: rows.map(x => x.toJSON()), itemCount: count, pageCount: Math.ceil(count / options.limit) }
        };
    }
    catch (error) {
        console.log({ step: "Models View", error: error.toString() });
        return { statusCode: "error", message: error.toString() };
    }
}
exports.findAndCountAll = findAndCountAll;
;
async function findOne(options) {
    try {
        const instance = await exports.Model.findOne(options);
        if (instance)
            return { statusCode: 'success', data: instance.toJSON() };
        else
            return { statusCode: 'notFound', message: "not Found" };
    }
    catch (error) {
        console.log({ step: "Models FindOne", error: error.toString() });
        return { statusCode: "error", message: error.toString() };
    }
}
exports.findOne = findOne;
;
async function update(values, options) {
    try {
        var options = { ...{ returning: true }, ...options };
        const instances = await exports.Model.update(values, options);
        return { statusCode: 'success', data: [instances[0], instances[1].map(x => x.toJSON())] };
    }
    catch (error) {
        console.log({ step: "Models update", error: error.toString() });
        return { statusCode: "error", message: error.toString() };
    }
}
exports.update = update;
;
async function SyncDB(params) {
    try {
        const model = await exports.Model.sync(params);
        return { statusCode: 'success', data: model.toString() };
    }
    catch (error) {
        console.log({ step: "Models SymcDb", error: error.toString() });
        return { statusCode: "error", message: error.toString() };
    }
    ;
}
exports.SyncDB = SyncDB;
//# sourceMappingURL=index.js.map