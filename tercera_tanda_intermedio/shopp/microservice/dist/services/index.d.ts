import * as T from "../types";
export declare function create(params: T.Services.Create.Request): Promise<T.Services.Create.Response>;
export declare function del(params: T.Services.Delete.Request): Promise<T.Services.Delete.Response>;
export declare function view(params: T.Services.View.Request): Promise<T.Services.View.Response>;
//# sourceMappingURL=index.d.ts.map