import Joi from 'joi';
import * as T from "./types";

export async function create(params: T.Create.Request): Promise<T.Create.Response> {

    try {

        const shcema = Joi.object({
            name: Joi.string().required(),
            mark: Joi.string().required(),
            year: Joi.date(),
            image: Joi.string().uri(),
            bodega: Joi.string(),
            lote: Joi.number()
        });

        const result = await shcema.validateAsync(params);

        return { statusCode: 'success', data: params };


    } catch (error) {

        throw { statusCode: "error", message: error.toString() }

    }

}

export async function del(params: T.Delete.Request): Promise<T.Delete.Response> {

    try {

        const shcema = Joi.object({
            ids: Joi.array().items(Joi.number().required()),
            marks: Joi.array().items(Joi.string().required()),
            years: Joi.array().items(Joi.date().required()),
            bodegas: Joi.array().items(Joi.string().required()),
            lotes: Joi.array().items(Joi.number().required()),

            state: Joi.boolean(),


        }).or('ids', 'marks', 'years', 'state', 'bodegas', 'lotes');

        const result = await shcema.validateAsync(params);

        return { statusCode: 'success', data: params };



    } catch (error) {

        throw { statusCode: "error", message: error.toString() }

    }
}

export async function update(params: T.Update.Request): Promise<T.Update.Response> {

    try {

        const shcema = Joi.object({
            id: Joi.number().required(),
            name: Joi.string(),
            mark: Joi.string(),
            year: Joi.date(),
            image: Joi.string().uri(),
            state: Joi.boolean(),
            bodega: Joi.string(),
            lote: Joi.number()
        });

        const result = await shcema.validateAsync(params);

        return { statusCode: 'success', data: params };

    } catch (error) {

        throw { statusCode: "error", message: error.toString() }

    }
}

export async function view(params: T.View.Request): Promise<T.View.Response> {

    try {

        const shcema = Joi.object({
            offset: Joi.number(),
            limit: Joi.number(),

            year: Joi.date(),
            state: Joi.boolean(),

            marks: Joi.array().items(Joi.string()),
            bodegas: Joi.array().items(Joi.string()),
            lotes: Joi.array().items(Joi.string()),
        });

        const result = await shcema.validateAsync(params);

        return { statusCode: "success", data: params };



    } catch (error) {

        throw { statusCode: "error", message: error.toString() };

    }
}

export async function findOne(params: T.FindOne.Request): Promise<T.FindOne.Response> {

    try {

        const shcema = Joi.object({
            id: Joi.number().required(),
        });

        const result = await shcema.validateAsync(params);

        return { statusCode: 'success', data: params };


    } catch (error) {

        throw { statusCode: "error", message: error.toString() };

    }
}