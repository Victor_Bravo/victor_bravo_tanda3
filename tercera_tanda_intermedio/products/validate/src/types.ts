type StatusCode = 'success' | 'error' | 'notFound' | 'notPermitted' | 'ValidationError';


export namespace FindOne {

    export interface Request {
        id: string;
    }

    export interface Response {
        statusCode: StatusCode;
        data?: Request;
        message?: string;
    }
}

export namespace View {

    export interface Request {
        offset?: number;
        limit?: number;

        year?: string;
        state?: boolean;

        marks?: string[];
        bodegas?: string[];
        lotes?: string[];
    }

    export interface Response {
        statusCode: StatusCode;
        data?: Request;
        message?: string;
    }
}

export namespace Update {

    export interface Request {
        id: number;
        name?: string;
        mark?: string;
        year?: string;
        image?: string;
        state?: boolean;
        bodega?: string;
        lote?: number;
    }

    export interface Response {
        statusCode: StatusCode;
        data?: Request;
        message?: string;
    }
}

export namespace Create {

    export interface Request {
        name: string;
        mark: string;
        year?: string;
        image?: string;
        bodega?: string;
        lote?: number;
    }

    export interface Response {
        statusCode: StatusCode;
        data?: Request;
        message?: string;
    }
}

export namespace Delete {

    export interface Request {
        ids?: number[];
        marks?: string[];
        years?: string[];
        state?: boolean;
        bodegas?: string[];
        lotes?: number[];
    }

    export interface Response {
        statusCode: StatusCode;
        data?: Request;
        message?: string;
    }
}

