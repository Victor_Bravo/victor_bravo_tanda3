"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.findOne = exports.view = exports.update = exports.del = exports.create = void 0;
const joi_1 = __importDefault(require("joi"));
async function create(params) {
    try {
        const shcema = joi_1.default.object({
            name: joi_1.default.string().required(),
            mark: joi_1.default.string().required(),
            year: joi_1.default.date(),
            image: joi_1.default.string().uri(),
            bodega: joi_1.default.string(),
            lote: joi_1.default.number()
        });
        const result = await shcema.validateAsync(params);
        return { statusCode: 'success', data: params };
    }
    catch (error) {
        throw { statusCode: "error", message: error.toString() };
    }
}
exports.create = create;
async function del(params) {
    try {
        const shcema = joi_1.default.object({
            ids: joi_1.default.array().items(joi_1.default.number().required()),
            marks: joi_1.default.array().items(joi_1.default.string().required()),
            years: joi_1.default.array().items(joi_1.default.date().required()),
            bodegas: joi_1.default.array().items(joi_1.default.string().required()),
            lotes: joi_1.default.array().items(joi_1.default.number().required()),
            state: joi_1.default.boolean(),
        }).or('ids', 'marks', 'years', 'state', 'bodegas', 'lotes');
        const result = await shcema.validateAsync(params);
        return { statusCode: 'success', data: params };
    }
    catch (error) {
        throw { statusCode: "error", message: error.toString() };
    }
}
exports.del = del;
async function update(params) {
    try {
        const shcema = joi_1.default.object({
            id: joi_1.default.number().required(),
            name: joi_1.default.string(),
            mark: joi_1.default.string(),
            year: joi_1.default.date(),
            image: joi_1.default.string().uri(),
            state: joi_1.default.boolean(),
            bodega: joi_1.default.string(),
            lote: joi_1.default.number()
        });
        const result = await shcema.validateAsync(params);
        return { statusCode: 'success', data: params };
    }
    catch (error) {
        throw { statusCode: "error", message: error.toString() };
    }
}
exports.update = update;
async function view(params) {
    try {
        const shcema = joi_1.default.object({
            offset: joi_1.default.number(),
            limit: joi_1.default.number(),
            year: joi_1.default.date(),
            state: joi_1.default.boolean(),
            marks: joi_1.default.array().items(joi_1.default.string()),
            bodegas: joi_1.default.array().items(joi_1.default.string()),
            lotes: joi_1.default.array().items(joi_1.default.string()),
        });
        const result = await shcema.validateAsync(params);
        return { statusCode: "success", data: params };
    }
    catch (error) {
        throw { statusCode: "error", message: error.toString() };
    }
}
exports.view = view;
async function findOne(params) {
    try {
        const shcema = joi_1.default.object({
            id: joi_1.default.number().required(),
        });
        const result = await shcema.validateAsync(params);
        return { statusCode: 'success', data: params };
    }
    catch (error) {
        throw { statusCode: "error", message: error.toString() };
    }
}
exports.findOne = findOne;
//# sourceMappingURL=index.js.map