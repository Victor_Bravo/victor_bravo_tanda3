
import * as S from 'sequelize';

type StatusCode = 'success' | 'error' | 'notFound' | 'notPermitted' | 'ValidationError';

export namespace Models {

    export interface ModelsAttributes {
        id?: number;

        name?: string;

        mark?: string;

        year?: string;

        image?: string;

        code?: string;

        state?: boolean;

        bodega?: string;

        lote?: number;

        createdAt?: string;

        updatedAt?: string;
    }

    export const attributes = ['id', 'name', 'mark', 'year', 'image', 'code', 'state', 'bodega', 'lote', 'createdAt', 'updatedAt'] as const;

    export type Attributes = typeof attributes[number];

    export type where = S.WhereOptions<ModelsAttributes>

    export interface Model extends S.Model<ModelsAttributes> {

    }

    export interface Paginate {
        data: ModelsAttributes[],
        itemCount: number,
        pageCount: number
    }

    export namespace Count {

        export interface Request extends Omit<S.CountOptions<ModelsAttributes>, "group"> {

        }

        export interface Response {

            statusCode: StatusCode;
            data?: number;
            message?: string;
        }
    }

    export namespace Create {

        export interface Request extends ModelsAttributes {

        }

        export interface Opts extends S.CreateOptions<ModelsAttributes> {

        }

        export interface Response {

            statusCode: StatusCode;
            data?: ModelsAttributes;
            message?: string;
        }

    }

    export namespace Del {

        export interface Opts extends S.DestroyOptions<ModelsAttributes> {

        }

        export interface Response {

            statusCode: StatusCode;
            data?: number;
            message?: string;
        }
    }
    export namespace FindAndCountAll {

        export interface Opts extends Omit<S.FindAndCountOptions<ModelsAttributes>, "group"> {

        }

        export interface Response {

            statusCode: StatusCode;
            data?: Paginate;
            message?: string;
        }

    }

    export namespace FindOne {

        export interface Opts extends S.FindOptions<ModelsAttributes> {

        }

        export interface Response {

            statusCode: StatusCode;
            data?: ModelsAttributes;
            message?: string;
        }

    }

    export namespace Update {

        export interface Request extends ModelsAttributes {

        }

        export interface Opts extends S.UpdateOptions<ModelsAttributes> {

        }

        export interface Response {

            statusCode: StatusCode;
            data?: [number, ModelsAttributes[]];
            message?: string;
        }

    }

    export namespace SyncDB {

        export interface Request extends S.SyncOptions {

        }

        export interface Response {

            statusCode: StatusCode;
            data?: string;
            message?: string;

        }

    }

}

export namespace Services {

    export namespace FindOne {

        export interface Request {
            id: string;
        }

        export interface Response {
            statusCode: StatusCode;
            data?: Models.ModelsAttributes;
            message?: string;
        }
    }

    export namespace View {

        export interface Request {
            offset?: number;
            limit?: number;

            year?: string;
            state?: boolean;

            marks?: string[];
            bodegas?: string[];
            lotes?: string[];
        }

        export interface Response {
            statusCode: StatusCode;
            data?: Models.Paginate;
            message?: string;
        }
    }

    export namespace Update {

        export interface Request {
            id: number;
            name?: string;
            mark?: string;
            year?: string;
            image?: string;
            state?: boolean;
            bodega?: string;
            lote?: number;
        }

        export interface Response {
            statusCode: StatusCode;
            data?: Models.ModelsAttributes;
            message?: string;
        }
    }

    export namespace Create {

        export interface Request {
            name: string;
            mark: string;
            year?: string;
            image?: string;
            bodega?: string;
            lote?: number;
        }

        export interface Response {
            statusCode: StatusCode;
            data?: Models.ModelsAttributes;
            message?: string;
        }
    }

    export namespace Delete {

        export interface Request {
            ids?: number[];
            marks?: string[];
            years?: string[];
            state?: boolean;
            bodegas?: string[];
            lotes?: number[];
        }

        export interface Response {
            statusCode: StatusCode;
            data?: number;
            message?: string;
        }
    }
}

export namespace Controller {

    export namespace Publish {

        export interface Request {
            channel: string;
            instance: string;
        }

        export interface Response {
            statusCode: StatusCode;
            data?: Request;
            message?: string;
        }
    }

}

export namespace Adapter {

    export const endpoint = ['create', 'password', 'delete', 'findOne', 'view', 'update'] as const;

    export type Endpoint = typeof endpoint[number];

    export namespace BullConn {
        export interface opts {
            concurrency: number;
            redis: Settings.REDIS;
        }
    }



}

export namespace Settings {

    export interface REDIS {

        host: string;
        port: number;
        password: string;

    }
}
