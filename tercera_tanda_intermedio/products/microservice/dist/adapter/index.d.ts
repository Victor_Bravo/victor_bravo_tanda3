import { Job } from 'bullmq';
import { Adapter } from "../types";
export declare const Process: (job: Job<any, any, Adapter.Endpoint>) => Promise<{
    statusCode: "error" | "success" | "notFound" | "notPermitted" | "ValidationError";
    data: import("../types").Models.ModelsAttributes;
    message: string;
} | {
    statusCode: "error" | "success" | "notFound" | "notPermitted" | "ValidationError";
    data: number;
    message: string;
} | {
    statusCode: "error" | "success" | "notFound" | "notPermitted" | "ValidationError";
    data: import("../types").Models.Paginate;
    message: string;
} | {
    statusCode: string;
    message: string;
    data?: undefined;
}>;
export declare const run: () => Promise<void>;
//# sourceMappingURL=index.d.ts.map