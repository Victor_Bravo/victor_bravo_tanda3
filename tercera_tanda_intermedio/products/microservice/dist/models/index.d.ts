import * as S from 'sequelize';
import { Models as T } from '../types';
export declare const Model: S.ModelCtor<T.Model>;
export declare function count(options?: T.Count.Request): Promise<T.Count.Response>;
export declare function create(values: T.Create.Request, options?: T.Create.Opts): Promise<T.Create.Response>;
export declare function del(options?: T.Del.Opts): Promise<T.Del.Response>;
export declare function findAndCountAll(options?: T.FindAndCountAll.Opts): Promise<T.FindAndCountAll.Response>;
export declare function findOne(options?: T.FindOne.Opts): Promise<T.FindOne.Response>;
export declare function update(values: T.Update.Request, options?: T.Update.Opts): Promise<T.Update.Response>;
export declare function SyncDB(params: T.SyncDB.Request): Promise<T.SyncDB.Response>;
//# sourceMappingURL=index.d.ts.map