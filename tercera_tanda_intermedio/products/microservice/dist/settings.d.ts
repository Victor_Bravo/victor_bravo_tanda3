import { createClient } from 'redis';
import { Sequelize } from 'sequelize';
import * as T from './types';
export declare const name: string;
export declare const version: number;
export declare const sequelize: Sequelize;
export declare const redisClient: ReturnType<typeof createClient>;
export declare const RedisOptsQueue: T.Adapter.BullConn.opts;
export declare const Action: {
    create: string;
    update: string;
    delete: string;
    orphan: string;
    error: string;
    start: string;
};
export declare const InternalError: string;
//# sourceMappingURL=settings.d.ts.map