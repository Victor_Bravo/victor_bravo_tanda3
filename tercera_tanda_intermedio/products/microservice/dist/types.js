"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Adapter = exports.Models = void 0;
var Models;
(function (Models) {
    Models.attributes = ['id', 'name', 'mark', 'year', 'image', 'code', 'state', 'bodega', 'lote', 'createdAt', 'updatedAt'];
})(Models = exports.Models || (exports.Models = {}));
var Adapter;
(function (Adapter) {
    Adapter.endpoint = ['create', 'password', 'delete', 'findOne', 'view', 'update'];
})(Adapter = exports.Adapter || (exports.Adapter = {}));
//# sourceMappingURL=types.js.map