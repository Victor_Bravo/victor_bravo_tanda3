import * as T from "../types";
export declare function create(params: T.Services.Create.Request): Promise<T.Services.Create.Response>;
export declare function del(params: T.Services.Delete.Request): Promise<T.Services.Delete.Response>;
export declare function update(params: T.Services.Update.Request): Promise<T.Services.Update.Response>;
export declare function view(params: T.Services.View.Request): Promise<T.Services.View.Response>;
export declare function findOne(params: T.Services.FindOne.Request): Promise<T.Services.FindOne.Response>;
//# sourceMappingURL=index.d.ts.map