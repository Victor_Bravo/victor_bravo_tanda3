"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.findOne = exports.view = exports.update = exports.del = exports.create = void 0;
const Models = __importStar(require("../models"));
const settings_1 = require("../settings");
const Validation = __importStar(require("validate-product"));
async function create(params) {
    try {
        await Validation.create(params);
        const { statusCode, data, message } = await Models.create(params);
        return { statusCode, data, message };
    }
    catch (error) {
        console.error({ step: "Service Create", error: error.toString() });
        return { statusCode: "error", message: settings_1.InternalError };
    }
}
exports.create = create;
async function del(params) {
    try {
        await Validation.del(params);
        var where = {};
        var optionals = ['id', 'mark', 'year', 'bodega', 'lote'];
        for (let x of optionals.map(x => x.concat('s')))
            if (params[x] !== undefined)
                where[x.slice(0, -1)] = params[x];
        if (params.state !== undefined)
            where.state = params.state;
        const { statusCode, data, message } = await Models.del({ where });
        if (statusCode !== 'success')
            return { statusCode, message };
        return { statusCode: 'success', data: data };
    }
    catch (error) {
        console.error({ step: "Service Delete", error: error.toString() });
        return { statusCode: "error", message: settings_1.InternalError };
    }
}
exports.del = del;
async function update(params) {
    try {
        await Validation.update(params);
        var where = { id: params.id };
        const findOne = await Models.findOne({ where });
        if (findOne.statusCode !== "success") {
            switch (findOne.statusCode) {
                case 'notFound': return { statusCode: "error", message: "producto no esta registrado" };
                default: return { statusCode: "error", message: settings_1.InternalError };
            }
        }
        const { statusCode, data, message } = await Models.update(params, { where });
        if (statusCode !== 'success')
            return { statusCode, message };
        return { statusCode: 'success', data: data[1][0] };
    }
    catch (error) {
        console.error({ step: "Service Delete", error: error.toString() });
        return { statusCode: "error", message: settings_1.InternalError };
    }
}
exports.update = update;
async function view(params) {
    try {
        await Validation.view(params);
        var where = {};
        var optionals = ['mark', 'bodega', 'lote'];
        for (let x of optionals.map(x => x.concat('s')))
            if (params[x] !== undefined)
                where[x.slice(0, -1)] = params[x];
        var optionals = ['state', 'year'];
        for (let x of optionals)
            if (params[x] !== undefined)
                where[x] = params[x];
        const { statusCode, data, message } = await Models.findAndCountAll({ where });
        return { statusCode, data, message };
    }
    catch (error) {
        console.error({ step: "Service Delete", error: error.toString() });
        return { statusCode: "error", message: settings_1.InternalError };
    }
}
exports.view = view;
async function findOne(params) {
    try {
        await Validation.findOne(params);
        var where = { id: params.id };
        const { statusCode, data, message } = await Models.findOne({ where });
        return { statusCode, data, message };
    }
    catch (error) {
        console.error({ step: "Service Delete", error: error.toString() });
        return { statusCode: "error", message: settings_1.InternalError };
    }
}
exports.findOne = findOne;
//# sourceMappingURL=index.js.map