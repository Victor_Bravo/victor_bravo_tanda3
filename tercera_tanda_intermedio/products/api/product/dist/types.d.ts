declare type StatusCode = 'success' | 'error' | 'notFound' | 'notPermitted' | 'ValidationError';
export declare const endpoint: readonly ["create", "password", "delete", "findOne", "view", "update"];
export declare type Endpoint = typeof endpoint[number];
export interface Paginate {
    data: Model[];
    itemCount: number;
    pageCount: number;
}
export interface Model {
    id: number;
    name: string;
    mark: string;
    year: string;
    image: string;
    state: boolean;
    bodega: string;
    lote: number;
    createdAt: string;
    updatedAt: string;
}
export declare namespace FindOne {
    interface Request {
        id: string;
    }
    interface Response {
        statusCode: StatusCode;
        data?: Model;
        message?: string;
    }
}
export declare namespace View {
    interface Request {
        offset?: number;
        limit?: number;
        year?: string;
        state?: boolean;
        marks?: string[];
        bodegas?: string[];
        lotes?: string[];
    }
    interface Response {
        statusCode: StatusCode;
        data?: Model;
        message?: string;
    }
}
export declare namespace Update {
    interface Request {
        id: number;
        name?: string;
        mark?: string;
        year?: string;
        image?: string;
        state?: boolean;
        bodega?: string;
        lote?: number;
    }
    interface Response {
        statusCode: StatusCode;
        data?: Model;
        message?: string;
    }
}
export declare namespace Create {
    interface Request {
        name: string;
        mark: string;
        year?: string;
        image?: string;
        bodega?: string;
        lote?: number;
    }
    interface Response {
        statusCode: StatusCode;
        data?: Model;
        message?: string;
    }
}
export declare namespace Delete {
    interface Request {
        ids?: number[];
        marks?: string[];
        years?: string[];
        state?: boolean;
        bodegas?: string[];
        lotes?: number[];
    }
    interface Response {
        statusCode: StatusCode;
        data?: number;
        message?: string;
    }
}
export interface REDIS {
    host: string;
    port: number;
    password: string;
}
export {};
//# sourceMappingURL=types.d.ts.map