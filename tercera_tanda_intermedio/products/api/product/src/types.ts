type StatusCode = 'success' | 'error' | 'notFound' | 'notPermitted' | 'ValidationError';

export const endpoint = ['create', 'password', 'delete', 'findOne', 'view', 'update'] as const;

export type Endpoint = typeof endpoint[number];

export interface Paginate {
    data: Model[],
    itemCount: number,
    pageCount: number
};

export interface Model {
    id: number;

    name: string;

    mark: string;

    year: string;

    image: string;

    state: boolean;

    bodega: string;

    lote: number;

    createdAt: string;

    updatedAt: string;
};


export namespace FindOne {

    export interface Request {
        id: string;
    }

    export interface Response {
        statusCode: StatusCode;
        data?: Model;
        message?: string;
    }
}

export namespace View {

    export interface Request {
        offset?: number;
        limit?: number;

        year?: string;
        state?: boolean;

        marks?: string[];
        bodegas?: string[];
        lotes?: string[];
    }

    export interface Response {
        statusCode: StatusCode;
        data?: Model;
        message?: string;
    }
}

export namespace Update {

    export interface Request {
        id: number;
        name?: string;
        mark?: string;
        year?: string;
        image?: string;
        state?: boolean;
        bodega?: string;
        lote?: number;
    }

    export interface Response {
        statusCode: StatusCode;
        data?: Model;
        message?: string;
    }
}

export namespace Create {

    export interface Request {
        name: string;
        mark: string;
        year?: string;
        image?: string;
        bodega?: string;
        lote?: number;
    }

    export interface Response {
        statusCode: StatusCode;
        data?: Model;
        message?: string;
    }
}

export namespace Delete {

    export interface Request {
        ids?: number[];
        marks?: string[];
        years?: string[];
        state?: boolean;
        bodegas?: string[];
        lotes?: number[];
    }

    export interface Response {
        statusCode: StatusCode;
        data?: number;
        message?: string;
    }
}



export interface REDIS {

    host: string;
    port: number;
    password: string;

};


