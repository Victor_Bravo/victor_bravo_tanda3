import * as user from 'users';
import * as product from 'product';
import * as shopp from 'shopp';


(async () => {

    try {

        await user.SyncDB({ force: true });
        await product.SyncDB({ force: true });
        await shopp.SyncDB({ force: true });


        user.run();
        product.run();
        shopp.run();


    } catch (error) { throw console.log(error); }
})()


