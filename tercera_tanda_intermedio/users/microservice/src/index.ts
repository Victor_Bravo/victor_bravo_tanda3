import { SyncDB } from "./models";

import { run } from "./adapter";

import { redisClient } from "./settings";

redisClient.on('error', (err) => console.log('Redis Client Error', err));


export { SyncDB, run };