import * as Models from "../models";
import { InternalError } from "../settings";
import * as T from "../types";

import * as Validation from 'validate-user'

export async function create(params: T.Services.Create.Request): Promise<T.Services.Create.Response> {

    try {

        await Validation.create(params);

        const findOne = await Models.findOne({ where: { username: params.username } });

        if (findOne.statusCode !== "notFound") {

            switch (findOne.statusCode) {
                case 'success': return { statusCode: "error", message: "Usuario ya existe" }

                default: return { statusCode: "error", message: InternalError }
            }
        }


        const { statusCode, data, message } = await Models.create(params);

        return { statusCode, data, message };

    } catch (error) {

        console.error({ step: "Service Create", error: error.toString() });

        return { statusCode: "error", message: InternalError }

    }

}

export async function del(params: T.Services.Delete.Request): Promise<T.Services.Delete.Response> {

    try {

        await Validation.del(params);

        var where: T.Models.where = { username: params.username }

        const findOne = await Models.findOne({ where });

        if (findOne.statusCode !== "success") {

            switch (findOne.statusCode) {
                case 'notFound': return { statusCode: "error", message: "Usuario no existe" }

                default: return { statusCode: "error", message: InternalError }
            }
        }


        const { statusCode, message } = await Models.del({ where });

        if (statusCode !== 'success') return { statusCode, message };
        return { statusCode: 'success', data: findOne.data }


    } catch (error) {

        console.error({ step: "Service Delete", error: error.toString() });

        return { statusCode: "error", message: InternalError }

    }
}

export async function update(params: T.Services.Update.Request): Promise<T.Services.Update.Response> {

    try {

        await Validation.update(params);

        var where: T.Models.where = { username: params.username }

        const findOne = await Models.findOne({ where });

        if (findOne.statusCode !== "success") {

            switch (findOne.statusCode) {
                case 'notFound': return { statusCode: "error", message: "Usuario no esta registrado" }

                default: return { statusCode: "error", message: InternalError }
            }
        }

        if (!findOne.data.state) {
            return { statusCode: "notPermitted", message: "Tu usuario no esta habilitado" };
        }



        const { statusCode, data, message } = await Models.update(params, { where });

        if (statusCode !== 'success') return { statusCode, message };
        return { statusCode: 'success', data: data[1][0] }


    } catch (error) {

        console.error({ step: "Service Delete", error: error.toString() });

        return { statusCode: "error", message: InternalError }

    }
}

export async function view(params: T.Services.View.Request): Promise<T.Services.View.Response> {

    try {

        await Validation.view(params);

        var where: T.Models.where = {};

        var optionals: T.Models.Attributes[] = ['state'];

        if (params.state !== undefined) where.state = params.state;

        for (let x of optionals) if (params[x] !== undefined) where[x] = params[x];

        const { statusCode, data, message } = await Models.findAndCountAll({ where });

        return { statusCode, data, message };



    } catch (error) {

        console.error({ step: "Service Delete", error: error.toString() });

        return { statusCode: "error", message: InternalError };

    }
}

export async function findOne(params: T.Services.FindOne.Request): Promise<T.Services.FindOne.Response> {

    try {

        await Validation.findOne(params);

        var where: T.Models.where = {}

        var optionals: T.Models.Attributes[] = ['id', 'username'];

        for (let x of optionals) if (params[x] !== undefined) where[x] = params[x];

        const { statusCode, data, message } = await Models.findOne({ where });

        return { statusCode, data, message }


    } catch (error) {

        console.error({ step: "Service Delete", error: error.toString() });

        return { statusCode: "error", message: InternalError };

    }
}