import dotenv from 'dotenv';
import { createClient } from 'redis'
import { Sequelize, Options } from 'sequelize';
import * as T from './types'

dotenv.config();

export const name: string = "user";
export const version: number = 1;


const REDIS: T.Settings.REDIS = {
    host: process.env.REDIS_HOST,
    port: parseInt(process.env.REDIS_PORT),
    password: process.env.REDIS_PASS
};

export const sequelize: Sequelize = new Sequelize({
    database: process.env.POSTGRES_DB,
    username: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    host: process.env.POSTGRES_HOST,
    port: parseInt(process.env.POSTGRES_PORT),
    logging: false,
    dialect: 'postgres',
})

export const redisClient: ReturnType<typeof createClient> = createClient({
    url: `redis://${REDIS.host}:${REDIS.port}`,
    password: REDIS.password
});

export const RedisOptsQueue: T.Adapter.BullConn.opts = {
    concurrency: parseInt(process.env.BULL_CONCURRENCY) || 50,
    redis: {
        host: REDIS.host,
        port: REDIS.port,
        password: REDIS.password,
    }
};

export const Action = {
    create: `${name}:create:${version}`,
    update: `${name}:update:${version}`,
    delete: `${name}:delete:${version}`,
    orphan: `${name}:orphan:${version}`,
    error: `${name}:error:${version}`,
    start: `${name}:start:${version}`
};

export const InternalError: string = "No podemos procesar tu solicitud en este momentos";