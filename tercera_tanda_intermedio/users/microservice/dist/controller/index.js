"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Publish = void 0;
const settings_1 = require("../settings");
const Publish = async (props) => {
    try {
        if (!settings_1.redisClient.isOpen)
            await settings_1.redisClient.connect();
        await settings_1.redisClient.publish(props.channel, props.instance);
        return { statusCode: "success", data: props };
    }
    catch (error) {
        console.error('error', { step: 'controller Publish', error });
        return { statusCode: "error", message: settings_1.InternalError };
    }
};
exports.Publish = Publish;
//todo: se puede extender en caso de querer conectarse con una libreria una api
//# sourceMappingURL=index.js.map