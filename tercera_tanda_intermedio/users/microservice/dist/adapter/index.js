"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.run = exports.Process = void 0;
const settings_1 = require("../settings");
const Services = __importStar(require("../services"));
const controller_1 = require("../controller");
const bullmq_1 = require("bullmq");
const Process = async (job) => {
    try {
        switch (job.name) {
            case 'create': {
                let { statusCode, data, message } = await Services.create(job.data);
                return { statusCode, data, message };
            }
            case 'delete': {
                let { statusCode, data, message } = await Services.del(job.data);
                return { statusCode, data, message };
            }
            case 'findOne': {
                let { statusCode, data, message } = await Services.findOne(job.data);
                return { statusCode, data, message };
            }
            case 'update': {
                let { statusCode, data, message } = await Services.update(job.data);
                return { statusCode, data, message };
            }
            case 'view': {
                let { statusCode, data, message } = await Services.view(job.data);
                return { statusCode, data, message };
            }
            default: return { statusCode: 'error', message: "Method not found" };
        }
        ;
    }
    catch (error) {
        await (0, controller_1.Publish)({ channel: settings_1.Action.error, instance: JSON.stringify({ step: 'Adaptador Process', message: error }) });
    }
};
exports.Process = Process;
const run = async () => {
    try {
        await (0, controller_1.Publish)({ channel: settings_1.Action.start, instance: JSON.stringify({ step: 'Adaptador Run', message: `starting ${settings_1.name}` }) });
        const worker = new bullmq_1.Worker(`${settings_1.name}:2`, exports.Process, { connection: settings_1.RedisOptsQueue.redis, concurrency: settings_1.RedisOptsQueue.concurrency });
        worker.on('error', async (error) => {
            await (0, controller_1.Publish)({ channel: settings_1.Action.error, instance: JSON.stringify({ step: 'Adapters run', error }) });
        });
    }
    catch (error) {
        await (0, controller_1.Publish)({ channel: settings_1.Action.error, instance: JSON.stringify({ step: 'Adaptador run', message: error }) });
    }
};
exports.run = run;
//# sourceMappingURL=index.js.map