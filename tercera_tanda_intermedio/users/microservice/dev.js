const { SyncDB, run } = require('./dist');

(async()=>{

        try {

                const result = await SyncDB({ force:false });
                
                if (result.statusCode !== "success") throw result.message;

                await run();
                
        } catch (error) { throw console.log(error);   }
})()


