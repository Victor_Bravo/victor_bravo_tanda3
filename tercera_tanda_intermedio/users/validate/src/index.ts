import Joi from 'joi';
import * as T from "./types";

export async function create(params: T.Create.Request): Promise<T.Create.Response> {

    try {

        const shcema = Joi.object({
            username: Joi.string().required(),
            fullName: Joi.string().required(),
            phone: Joi.number().required(),
            image: Joi.string().uri().required(),
        });

        const result = await shcema.validateAsync(params);

        if (params.username === 'admin' || 'root' || 'su') {
            throw { statusCode: "error", message: "Nombre reservado para uso interno" }
        }

        return { statusCode: 'success', data: params };


    } catch (error) {

        throw { statusCode: "error", message: error.toString() }

    }

}

export async function del(params: T.Delete.Request): Promise<T.Delete.Response> {

    try {

        const shcema = Joi.object({
            username: Joi.string().required(),
        });

        const result = await shcema.validateAsync(params);

        return { statusCode: 'success', data: params };



    } catch (error) {

        throw { statusCode: "error", message: error.toString() }

    }
}

export async function update(params: T.Update.Request): Promise<T.Update.Response> {

    try {

        const shcema = Joi.object({
            username: Joi.string().required(),
            fullName: Joi.string(),
            phone: Joi.number(),
            image: Joi.string().uri(),
        });

        const result = await shcema.validateAsync(params);

        return { statusCode: 'success', data: params };

    } catch (error) {

        throw { statusCode: "error", message: error.toString() }

    }
}

export async function view(params: T.View.Request): Promise<T.View.Response> {

    try {

        const shcema = Joi.object({
            offset: Joi.number(),
            limit: Joi.number(),
            state: Joi.boolean(),
        });

        const result = await shcema.validateAsync(params);

        return { statusCode: "success", data: params };



    } catch (error) {

        throw { statusCode: "error", message: error.toString() };

    }
}

export async function findOne(params: T.FindOne.Request): Promise<T.FindOne.Response> {

    try {

        const shcema = Joi.object({
            username: Joi.string(),
            id: Joi.string(),
        }).xor('username', 'id');

        const result = await shcema.validateAsync(params);

        return { statusCode: 'success', data: params };


    } catch (error) {

        throw { statusCode: "error", message: error.toString() };

    }
}