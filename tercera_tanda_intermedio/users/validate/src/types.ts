type StatusCode = 'success' | 'error' | 'notFound' | 'notPermitted' | 'ValidationError';

export namespace FindOne {

    export interface Request {
        username?: string;
        id?: string;
    }

    export interface Response {
        statusCode: StatusCode;
        data?: Request;
        message?: string;
    }
}

export namespace View {

    export interface Request {
        offset?: number;
        limit?: number;
        state?: boolean;
    }

    export interface Response {
        statusCode: StatusCode;
        data?: Request;
        message?: string;
    }
}

export namespace Update {

    export interface Request {
        username: string;
        fullName?: string;
        phone?: number;
        image?: string;
    }

    export interface Response {
        statusCode: StatusCode;
        data?: Request;
        message?: string;
    }
}

export namespace Create {

    export interface Request {
        username: string;
        fullName: string;
        phone: number;
        image: string;
    }

    export interface Response {
        statusCode: StatusCode;
        data?: Request;
        message?: string;
    }
}

export namespace Delete {

    export interface Request {
        username: string;
    }

    export interface Response {
        statusCode: StatusCode;
        data?: Request;
        message?: string;
    }
}
