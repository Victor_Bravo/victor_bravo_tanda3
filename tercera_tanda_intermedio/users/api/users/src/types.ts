type StatusCode = 'success' | 'error' | 'notFound' | 'notPermitted' | 'ValidationError';

export const endpoint = ['create', 'password', 'delete', 'findOne', 'view', 'update'] as const;

export type Endpoint = typeof endpoint[number];

export interface Paginate {
    data: Model[],
    itemCount: number,
    pageCount: number
};

export interface Model {
    id?: number;

    fullName?: string;

    username?: string;

    password?: string;

    state?: boolean;

    image?: string;

    phone?: number;

    createdAt?: string;

    updatedAt?: string;
};


export namespace FindOne {

    export interface Request {
        username?: string;
        id?: string;
    }

    export interface Response {
        statusCode: StatusCode;
        data?: Model;
        message?: string;
    }
}

export namespace View {

    export interface Request {
        offset?: number;
        limit?: number;
        state?: boolean;
    }

    export interface Response {
        statusCode: StatusCode;
        data?: Paginate;
        message?: string;
    }
}

export namespace Update {

    export interface Request {
        username: string;
        fullName?: string;
        phone?: number;
        image?: string;
    }

    export interface Response {
        statusCode: StatusCode;
        data?: Model;
        message?: string;
    }
}

export namespace Create {

    export interface Request {
        username: string;
        fullName: string;
        phone: number;
        image: string;
    }

    export interface Response {
        statusCode: StatusCode;
        data?: Model;
        message?: string;
    }
}

export namespace Delete {

    export interface Request {
        username: string;
    }

    export interface Response {
        statusCode: StatusCode;
        data?: Model;
        message?: string;
    }
};

export interface REDIS {

    host: string;
    port: number;
    password: string;

};


