export declare const redis: {
    host: string;
    port: number;
    password: string;
};
export declare const InternalError: string;
//# sourceMappingURL=settings.d.ts.map