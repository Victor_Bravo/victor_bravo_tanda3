"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const settings_1 = require("./settings");
const express_1 = __importDefault(require("express"));
const http_1 = __importDefault(require("http"));
const socket_io_1 = require("socket.io");
const apiUser = __importStar(require("api-users"));
const apiProduct = __importStar(require("api-product"));
const apiShopp = __importStar(require("api-shopp"));
const app = (0, express_1.default)();
const server = http_1.default.createServer(app);
const io = new socket_io_1.Server(server);
server.listen(3000, () => {
    console.log("Server Initialize");
    io.on('connection', socket => {
        console.log("new conection", socket.id);
        //* User
        socket.on('req:user:view', async (params) => {
            try {
                console.log("res:user:view");
                const { statusCode, data, message } = await apiUser.View(params, settings_1.redis);
                return io.to(socket.id).emit('res:user:view', { statusCode, data, message });
            }
            catch (error) {
                console.log(error);
            }
        });
        socket.on('req:user:create', async (params) => {
            try {
                console.log("res:user:create");
                const { statusCode, data, message } = await apiUser.Create(params, settings_1.redis);
                return io.to(socket.id).emit('res:user:create', { statusCode, data, message });
            }
            catch (error) {
                console.log(error);
            }
        });
        //* Product
        socket.on('req:product:view', async (params) => {
            try {
                console.log("res:product:view");
                const { statusCode, data, message } = await apiUser.View(params, settings_1.redis);
                return io.to(socket.id).emit('res:product:view', { statusCode, data, message });
            }
            catch (error) {
                console.log(error);
            }
        });
        socket.on('req:product:create', async (params) => {
            try {
                console.log("res:product:create");
                const { statusCode, data, message } = await apiProduct.Create(params, settings_1.redis);
                return io.to(socket.id).emit('res:product:create', { statusCode, data, message });
            }
            catch (error) {
                console.log(error);
            }
        });
        socket.on('req:product:delete', async (params) => {
            try {
                console.log("res:product:delete");
                const { statusCode, data, message } = await apiProduct.Delete(params, settings_1.redis);
                return io.to(socket.id).emit('res:product:delete', { statusCode, data, message });
            }
            catch (error) {
                console.log(error);
            }
        });
        //* shopp
        socket.on('req:shopp:view', async (params) => {
            try {
                console.log("res:shopp:view");
                const { statusCode, data, message } = await apiShopp.View(params, settings_1.redis);
                return io.to(socket.id).emit('res:shopp:view', { statusCode, data, message });
            }
            catch (error) {
                console.log(error);
            }
        });
        socket.on('req:shopp:create', async (params) => {
            try {
                console.log("res:shopp:create");
                const { statusCode, data, message } = await apiShopp.Create(params, settings_1.redis);
                return io.to(socket.id).emit('res:shopp:create', { statusCode, data, message });
            }
            catch (error) {
                console.log(error);
            }
        });
        socket.on('req:shopp:delete', async (params) => {
            try {
                console.log("res:shopp:delete");
                const { statusCode, data, message } = await apiShopp.Delete(params, settings_1.redis);
                return io.to(socket.id).emit('res:shopp:delete', { statusCode, data, message });
            }
            catch (error) {
                console.log(error);
            }
        });
    });
});
//# sourceMappingURL=index.js.map