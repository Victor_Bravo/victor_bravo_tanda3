import dotenv from 'dotenv';

dotenv.config();

export const redis: { host: string, port: number, password: string } = {
    host: process.env.REDIS_HOST,
    port: parseInt(process.env.REDIS_PORT),
    password: process.env.REDIS_PASS
};

export const InternalError: string = "No podemos procesar tu solicitud en este momentos";