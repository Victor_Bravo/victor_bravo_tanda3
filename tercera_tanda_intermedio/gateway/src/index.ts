import { Create } from './../../shopp/api/shopp/src/index';
import { redis } from './settings';
import express, { Express } from "express";
import http from 'http';
import { Server } from 'socket.io'
import * as apiUser from 'api-users';
import * as apiProduct from 'api-product';
import * as apiShopp from 'api-shopp';

const app: Express = express();

const server: http.Server = http.createServer(app);

const io = new Server(server);

server.listen(3000, () => {

    console.log("Server Initialize");

    io.on('connection', socket => {

        console.log("new conection", socket.id);

        //* User

        socket.on('req:user:view', async (params: apiUser.T.View.Request) => {
            try {
                console.log("res:user:view");

                const { statusCode, data, message } = await apiUser.View(params, redis);

                return io.to(socket.id).emit('res:user:view', { statusCode, data, message })
            } catch (error) {
                console.log(error);
            }
        });

        socket.on('req:user:create', async (params: apiUser.T.Create.Request) => {
            try {
                console.log("res:user:create");

                const { statusCode, data, message } = await apiUser.Create(params, redis);

                return io.to(socket.id).emit('res:user:create', { statusCode, data, message })
            } catch (error) {
                console.log(error);
            }
        });


        //* Product

        socket.on('req:product:view', async (params: apiProduct.T.View.Request) => {
            try {
                console.log("res:product:view");

                const { statusCode, data, message } = await apiUser.View(params, redis);

                return io.to(socket.id).emit('res:product:view', { statusCode, data, message })
            } catch (error) {
                console.log(error);
            }
        });

        socket.on('req:product:create', async (params: apiProduct.T.Create.Request) => {
            try {
                console.log("res:product:create");

                const { statusCode, data, message } = await apiProduct.Create(params, redis);

                return io.to(socket.id).emit('res:product:create', { statusCode, data, message })
            } catch (error) {
                console.log(error);
            }
        });

        socket.on('req:product:delete', async (params: apiProduct.T.Delete.Request) => {
            try {
                console.log("res:product:delete");

                const { statusCode, data, message } = await apiProduct.Delete(params, redis);

                return io.to(socket.id).emit('res:product:delete', { statusCode, data, message })
            } catch (error) {
                console.log(error);
            }
        });



        //* shopp

        socket.on('req:shopp:view', async (params: apiShopp.T.View.Request) => {
            try {
                console.log("res:shopp:view");

                const { statusCode, data, message } = await apiShopp.View(params, redis);

                return io.to(socket.id).emit('res:shopp:view', { statusCode, data, message })
            } catch (error) {
                console.log(error);
            }
        });

        socket.on('req:shopp:create', async (params: apiShopp.T.Create.Request) => {
            try {
                console.log("res:shopp:create");

                const { statusCode, data, message } = await apiShopp.Create(params, redis);

                return io.to(socket.id).emit('res:shopp:create', { statusCode, data, message })
            } catch (error) {
                console.log(error);
            }
        });

        socket.on('req:shopp:delete', async (params: apiShopp.T.Delete.Request) => {
            try {
                console.log("res:shopp:delete");

                const { statusCode, data, message } = await apiShopp.Delete(params, redis);

                return io.to(socket.id).emit('res:shopp:delete', { statusCode, data, message })
            } catch (error) {
                console.log(error);
            }
        });

    })
})