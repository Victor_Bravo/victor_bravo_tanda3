import React, { useEffect, useState } from "react";
import {socket} from "./ws";
import styled from "styled-components";

const Container = styled.div`
    margin-top: 30px;
    width: 75%;
    max-width: 100%;
    height: calc(50vh);
    overflow: scroll;

`;

const Socio = styled.div`
    margin-top: 15px;
    display: flex;
    flex-direction: column;
    background-color:#303536;
    border-radius:15px;
    color: white;
    padding-bottom: 10px;
    

`;

const Body = styled.div`
    padding-left: 15px;
    padding-right: 15px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
`;

const Name = styled.p`
`;

const Phone = styled.p`

`;

const Email = styled.p`

`;

const Enable = styled.div`
    width: 25px;
    height: 25px;
    border-radius: 50%;
    background-color: ${ props => props.enable ? 'green' : 'red' };

`;

const Button = styled.button`
    background-color: #e71414a1;
    padding: 10px 20px 10px 20px;
    border-radius: 15px;
    opacity: 0.8;
    font-weight: bold;
    font-size: 1rem;
    color: white;
    top: 20px;
    left: 20px;
    position: relative;
    margin-bottom: 10px;

`;

const Icon = styled.img`
    cursor: pointer;
    width: 35px;
    height: 35px;
`;
const App = () => {
    const [data, setData] = useState([]);

	useEffect(()=>{

		socket.on('res:socios:view',({ statusCode, data, message })=>{

			console.log('res:socios:view',{ statusCode, data, message });
			
			console.log({statusCode, data, message});
			
			if(statusCode === 200) setData(data);
			
		});
        setTimeout(() => socket.emit('req:socios:view',({ })), 1000);


	},[])		
    const handleCreate = () =>{
        socket.emit('req:socios:create',{name:"Victor Manuel", phone:"0424-6783452"})
    }
    const handleDelete = (id) =>{
        socket.emit('req:socios:delete',{id})
    }

    const handleChange = (id,status) =>{
        console.log(status);
        if(status) socket.emit('req:socios:disable',{id});

        else socket.emit('req:socios:enable',{id})}

    return(
        <>            
        <Button  onClick={handleCreate}>Create</Button>

        <Container>

			{
            
            data.map((x, i) => (
            <Socio>

                <Body>
                <Icon src="https://cdn-icons-png.flaticon.com/512/458/458594.png" onClick= {()=>handleDelete(x.id)} />
                    <Name>{x.name} 
                    <small> Numero de Socio: {x.id}</small>
                    </Name>
                    <Phone>{x.phone}</Phone>
                </Body> 

                <Body>
                    <Email>{x.email}</Email>
                    <Enable enable={x.enable} onClick={()=>handleChange(x.id, x.enable)} />
                </Body> 
            </Socio>           
                ))    
                }
			
        </Container>
        </>
    )
}

export default App;