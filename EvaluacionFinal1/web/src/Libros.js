import React, { useEffect, useState } from "react";
import {socket} from "./ws";
import styled from "styled-components";

const Container = styled.div`

    background-color: black;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-around;
    width: calc(100%);
    display: flex;
`;

const Card = styled.div`
    
    background-color:#464646 ;
    width: 45%;
    height: 450px;
    margin-bottom: 10px;
    border-radius:10px;
    color: white;
    padding: 2px;
    display: block;
    margin-top: 80px;

`;

const Portada = styled.img`

    width: 90%;
    margin-top: 10px;
    margin-left: 10px;
    height: 250px;
    border-radius: 15px;
`;

const Icon = styled.img`
    cursor: pointer;
    position: relative;
    opacity: calc(65%);
    top: 0;
    margin: 5px;
    left: 0px;
    margin-left: auto;
    width: 35px;
    height: 35px;
`;

const Icon2 = styled.img`
    cursor: pointer;
    position: relative;
    width: 35px;
    height: 35px;
    margin-left: 40px;
    margin-top: auto;

`;
const Button = styled.button`
    background-color: #036e00;
    padding: 10px 20px 10px 20px;
    border-radius: 15px;
    font-weight: bold;
    font-size: 1rem;
    color: white;
    font-size: 25px;
    position: static;
    margin-top: 20px;
    text-align: center;
    width: 100%;
`

const ContainerD = styled.div`
width: 50%;
color: white;
`
const ContainerC = styled.div`
padding: 10px;
margin-left:auto;
margin-right: auto;
display: flex;
width: 97%;

`

const ContainerA = styled.div`
display: flex;
flex-direction: row;
margin-bottom: 20px;
padding-bottom: 20px;
border-bottom: 2px solid white;

`

const Input = styled.input`
height: 20px;
width: 160px;
margin-top: 28px;
padding-left: 10px;
margin-bottom: 0px;
border-color: white;
border-radius: 5px;
`

const Title = styled.h1`    
    text-align: center;
`

const SubTitle = styled.h2`

`
const Item = ({ image, brand, model, plaque, id, color, description, idc, level }) =>{
var estado;
    if (!description) {
        description="El auto esta en perfecto estado"
        estado="green";
    } else{
        estado="red"
    }
    
    
    const handleDeleteProblem = () =>socket.emit('req:problema:delete',{id:id});
    const handleDelete = () =>socket.emit('req:autos:delete',{id:idc});
    const handleCreateProblem = ()=>{
        let nivel = document.getElementById(`des ${idc}`).value;
        let des = document.getElementById(`level ${idc}`).value;
        if (!nivel || !des) alert("campos vacios");
        else socket.emit('req:problema:create',{level:nivel, auto:idc, description:des});

    }
    const opts = {
        src:"https://cdn-icons-png.flaticon.com/512/458/458594.png",
        onClick: handleDelete
    };

    const opts3 = {
        src:"https://cdn-icons-png.flaticon.com/512/458/458594.png",
        onClick: handleDeleteProblem
    };

    const opts2 = {
        src:"https://img.icons8.com/fluency/2x/plus.png",
        onClick: handleCreateProblem
    };
   if (estado ==="red") {
    return(
        <Card>
            
            <ContainerA>
            <ContainerD><Portada src={image} alt={`imagen de ${image}`} /></ContainerD>
            <ContainerD>
            <Title>
                {brand}
            </Title>
            <SubTitle>
               Modelo: {model}
            </SubTitle>
            <SubTitle>
               Color: {color}
            </SubTitle>
            <SubTitle>
               Placa: {plaque}
            </SubTitle>
            </ContainerD>
            <Icon {...opts} />

            </ContainerA>
            <ContainerC style={{background: "rgb(252,70,107)",background:" radial-gradient(circle, rgba(252,70,107,1) 0%, rgba(232,10,10,1) 100%)"}}  >
                <SubTitle >
                    Problemas: {level} <br/>
                    nivel: { description }
                </SubTitle>
                <Icon {...opts3} />
            </ContainerC>
        </Card>
        
)
   } else{
    return(
<Card>
            
            <ContainerA>
            <ContainerD><Portada src={image} /></ContainerD>
            <ContainerD>
            <Title>
                {brand}
            </Title>
            <SubTitle>
               Modelo: {model}
            </SubTitle>
            <SubTitle>
               Color: {color}
            </SubTitle>
            <SubTitle>
               Placa: {plaque}
            </SubTitle>
            </ContainerD>
            <Icon {...opts} />

            </ContainerA>
            <ContainerC style={{background: "rgb(14,157,18)",background:"radial-gradient(circle, rgba(14,157,18,1) 0%, rgba(4,40,0,1) 100%)"}}  >
              <div style={{position:"absolute",display:"flex", alignItems:"center",justifyContent:"space-around", flexDirection: "row",flexWrap: "wrap"}}>  <Input style={{marginLeft:40}} placeholder="Descripcion" id={`des ${idc}`}/>  <Input style={{marginLeft:40}} placeholder="Nivel" id={`level ${idc}`} type="number" max="3" min="0" /> <Icon2 {...opts2} /> </div>
                <SubTitle style={{paddingTop:50 , marginLeft:40}} >
                { description } 
                </SubTitle>            </ContainerC>
        </Card>
    )
}
}


const App= () => {

    const [data, setData] = useState([]);

	useEffect(()=>{

		socket.on('res:autos:view',({ statusCode, data, message })=>{

			console.log('res:autos:view',{ statusCode, data, message });
			
			console.log({statusCode, data, message});
			
			if(statusCode === 200) setData(data);
			
		});

        setTimeout(() => socket.emit('req:autos:view',({})), 1000);

	},[])

    const handleCreate = () =>{
        let imagen = document.getElementById("image").value;
        let modelo = document.getElementById("model").value;
        let marca = document.getElementById("brand").value;
        let placa = document.getElementById("plaque").value;
        let colorw = document.getElementById("color").value;
        if (!modelo || !marca || !placa || !colorw) alert(`campo vacio ${imagen}`) 
        else socket.emit('req:autos:create',{image:imagen, brand:marca, model:modelo, plaque: placa, color: colorw });
    }

    

    return(

        <Container>
            
            <Input type="text" id="brand" placeholder="Marca"/>            
            <Input type="text" id="model" placeholder="Modelo" /> 
            <Input type="text" id="color" placeholder="Color" />            
            <Input type="text" id="plaque" placeholder="Placa" /> 
            <Input type="text" id="image" placeholder="Imagen"/>            

            <Button  onClick={handleCreate}>Create</Button>
            {

                data.map((x,i)=>( <Item {...x}  />))

            }
        </Container>
    )

}

export default App;