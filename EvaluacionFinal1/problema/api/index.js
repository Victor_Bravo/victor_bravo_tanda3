const bull = require("bull");
const { name } = require("./package.json")
const redis = {
  host: "localhost",
  port: 6379,
};const opts = {
  redis: { host: redis.host, port: redis.port },
};
const queueCreate = bull(`${name.replace('-api', '')}:create`, opts);

const queueDelete = bull(`${name.replace('-api', '')}:delete`, opts);

const queueUpdate = bull(`${name.replace('-api', '')}:update`, opts);

const queueFindOne = bull(`${name.replace('-api', '')}:findOne`, opts);

const queueView = bull(`${name.replace('-api', '')}:View`, opts);

async function Create({ level, auto, description }) {
  try {

    const job = await queueCreate.add({ level, auto, description });

    const {statusCode, data, message} = await job.finished();

    return {statusCode, data, message}

} catch (error) {

  console.log(error);

}
}
async function Delete({ id }) {

  try {

    const job = await queueDelete.add({ id });

    const {statusCode, data, message} = await job.finished();

    return {statusCode, data, message};

  } catch (error) {

      console.log(error);
  }
}

async function Update({ id, level, auto, description }) {

  try {


    const job = await queueUpdate.add({ id, level, auto, description });

    const {statusCode, data, message} = await job.finished();

    return {statusCode, data, message};

    // console.log({statusCode, data, message});

  } catch (error) {

    console.log(error);

  }
}

async function FindOne({ id }) {

  try {


    const job = await queueFindOne.add({ id });

    const {statusCode, data, message} = await job.finished();

    return {statusCode, data, message};

  } catch (error) {

    console.log(error);

  }

}

async function View({ }) {

  try {

    const job = await queueView.add({ });

    const {statusCode, data, message} = await job.finished();

    return {statusCode, data, message};

  } catch (error) {

    console.log(error);

  }

}

async function main() {

  //await Create({name:"Spiderman", age:23, color:"Verde"})

  //  await Delete({id: 5});
  // await Update({name:"Jorge", id:9})
  // await FindOne({id:9}); 
  await View({});

}

module.exports = { Create, Delete, Update, FindOne, View }