const api = require('./api');

async function main() {
    
    try {
      let {data, statusCode, message} = await api.Create({level:"1", auto:4, description:"transmicion"});

      console.log({data, statusCode, message});
    } catch (error) {

        console.error(error);
        
    }
   

}

async function view() {
    
  try {
    let {data, statusCode, message} = await api.View({enable:true});

    console.log({data, statusCode, message});
  } catch (error) {

      console.error(error);
      
  }
 

}
async function dele() {
    
  try {
    let {data, statusCode, message} = await api.Delete({id:3});

    console.log({data, statusCode, message});
  } catch (error) {

      console.error(error);
      
  }

}
async function find() {
  try {
    let {data, statusCode, message} = await api.FindOne({id:1});
    console.log({data, statusCode, message});
  } catch (error) {
    console.log(error);
  }
  
}
view();