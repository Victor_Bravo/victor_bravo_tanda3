const { Model } = require("../Models");

async function Create({ level, auto, description }) {
  try {

    let instance = await Model.create(
      { level, auto, description }, {logging:false }
    );

    return { statusCode: 200, data: instance.toJSON() };
  
  } catch (error) {
  
    console.log({ step: "Controllers Create", error: error.toString() });
  
    return { statusCode: 400, message: error.toString() };
  
  }
}

async function Delete({where = {}}) {
  try {

    await Model.destroy({ where, logging:false });
   
    return { statusCode: 200, date: "ok" };
  
  } catch (error) {
  
    console.log({ step: "Controllers Delete", error: error.toString() });
  
    return { statusCode: 400, message: error.toString() };
  }
}

async function Update({ id, level, auto, description }) {
  try {

    let instance = await Model.update({ id, level, auto, description  }, { where: { id }, 
    
    logging:false, returning:true });
    
    return { statusCode: 200, data: instance[1][0].toJSON() };
  } catch (error) {
    
    console.log({ step: "Controllers Update", error: error.toString() });
    
    return { statusCode: 400, message: error.toString() };
  }
}

async function FindOne({ where = {auto} }) {
  try {
    let instance = await Model.findOne({ where });

    if(instance) return { statusCode: 200, data: instance.toJSON() };

    else return {statusCode:400, message:"No Existe El Usuario"}

  } catch (error) {

    console.log({ step: "Controllers FindOne", error: error.toString() });

    return { statusCode: 500, message: error.toString() };
  }
}

async function View({ where = {} }) {
  try {

    let instance = await Model.findAll({ where, logging:false });
    
    return { statusCode: 200, data: instance };
  } catch (error) {
    
    console.log({ step: "Controllers View", error: error.toString() });
    
    return { statusCode: 500, message: error.toString() };
  }
}

module.exports = { Create, Delete, Update, FindOne, View };
