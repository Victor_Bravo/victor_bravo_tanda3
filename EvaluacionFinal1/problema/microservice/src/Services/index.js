const Controllers = require("../Controllers");
const { InternalError } = require("../settings");
const apiAutos = require("api-autos");

async function Create({ level, auto, description }) {
  try {
  
    const validarAuto = await apiAutos.FindOne({id:auto});

  if (validarAuto.statusCode !== 200) {
      switch (validarAuto.statusCode) {
        case 400: return {statusCode:400, message:"No existe el auto"}
      
        default: return {statusCode:500, message: InternalError}
      }
  }

    let { statusCode, data, message } = await Controllers.Create({
      level, 
      auto, 
      description,
    });

    return { statusCode, data, message };
  
  } catch (error) {
  
    console.log({ step: "service Create", error: error.toString() });

    return { statusCode: 500, message: error.toString() };
  }
}

async function Delete({ id }) {
  try {
    const findOne= await Controllers.FindOne({
      where: { id },
    });

    if (findOne.statusCode !== 200) {

      switch (findOne.statusCode) {
        case 400:return {statusCode:400, message:"No existe el usuario a eliminar"};

        default:return {statusCode:500, message: InternalError};
      }

    }

    const del= await Controllers.Delete({where: { id }});

    if(del.statusCode === 200) return { statusCode:200, data: findOne.data};

    return {statusCode:400, message: InternalError}

  } catch (error) {

    console.log({ step: "service Delete", error: error.toString() });

    return { statusCode: 500, message: error.toString() };
  }
}

async function Update({ id, level, auto, description }) {
  try {
    let { statusCode, data, message } = await Controllers.Update({
      id,
      level, 
      auto, 
      description,
    });

    return { statusCode, data, message };

  } catch (error) {
    console.log({ step: "service Update", error: error.toString() });

    return { statusCode: 500, message: error.toString() };
  }
}
async function FindOne({ auto }) {
  try {
    
    let { statusCode, data, message } = await Controllers.FindOne({
      where: { auto },
    });
    
    return { statusCode, data, message };
  
  } catch (error) {
  
    console.log({ step: "service FindOne", error: error.toString() });

    return { statusCode: 500, message: error.toString() };
  }
}
async function View({  }) {
  

  try {
  
    let { statusCode, data, message } = await Controllers.View({ });
  
    return { statusCode, data, message };
  
  } catch (error) {
  
    console.log({ step: "service View", error: error.toString() });

    return { statusCode: 500, message: error.toString() };
  }
}


module.exports = { Create, Delete, Update, FindOne, View };

