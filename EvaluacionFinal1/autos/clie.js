const api = require('./api');

async function main() {
    
    try {
      let {data, statusCode, message} = await api.Update({ id:15, color:"verde"});

      console.log({data, statusCode, message});

    } catch (error) {

        console.error(error);
        
    }
   

}

async function find() {
  try {
    let {data, statusCode, message} = await api.FindOne({id:8});
    console.log({data, statusCode, message});
  } catch (error) {
    console.log(error);
  }
  
}

async function view() {
    
  try {
    let {data, statusCode, message} = await api.View({ });

    console.log({data, statusCode, message});
  } catch (error) {

      console.error(error);
      
  }
 

}
async function dele() {
    
  try {
    let {data, statusCode, message} = await api.Delete({id:8});

    console.log({data, statusCode, message});
  } catch (error) {

      console.error(error);
      
  }

}
main();