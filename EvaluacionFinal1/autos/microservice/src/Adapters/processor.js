const Services = require("../Services");
const { InternalError } = require("../settings");
const {
  queueCreate,
  queueView,
  queueDelete,
  queueUpdate,
  queueFindOne,
} = require("./index");

async function View(job, done) {
  const { } = job.data;
  try {
    let { statusCode, data, message } = await Services.View({ });

    done(null, { statusCode, data:data, message });
  } catch (error) {
    console.log({ step: "service Adaptador", error: error.toString() });

    done(null, { statusCode: 500, message: InternalError });
  }
}
async function Create(job, done) {
  try {

    const { brand, model, color, plaque, image } = job.data;

    let { statusCode, data, message } = await Services.Create({
      brand, 
      model, 
      color, 
      plaque,
      image,
    });

    done(null, { statusCode, data, message });
  } catch (error) {
    console.log({ step: "Adaptador queueCreate", error: error.toString() });

    done(null, { statusCode: 500, message: InternalError });
  }
}
async function Delete(job, done) {
  try {

    const { id } = job.data;

    let { statusCode, data, message } = await Services.Delete({ id });

    done(null, { statusCode, data, message });

  } catch (error) {
    console.log({ step: "Adaptador queueDelete", error: error.toString() });

    done(null, { statusCode: 500, message: InternalError });
  }
}
async function Update(job, done) {
  try {
    const { id, brand, model, color, plaque, image } = job.data;

    let { statusCode, data, message } = await Services.Update({
      id, 
      brand, 
      model, 
      color, 
      plaque,
      image,
    });

    done(null, { statusCode, data, message });
  } catch (error) {
    console.log({ step: "Adaptador Update", error: error.toString() });

    done(null, { statusCode: 500, message: InternalError });
  }
}
async function FindOne(job, done) {

  try {

    const { id } = job.data;

    let { statusCode, data, message } = await Services.FindOne({ id });

    done(null, { statusCode, data, message });
  } catch (error) {
    console.log({ step: "service Adaptador", error: error.toString() });

    done(null, { statusCode: 500, message: InternalError });
  }
}


async function run() {
  try {
    console.log("Vamos a inicializar worker");
    queueView.process(View);

    queueCreate.process(Create);
    
    queueDelete.process(Delete);
    
    queueUpdate.process(Update);
    
    queueFindOne.process(FindOne);

  } catch (error) {

    console.log("esto es un error" + error);

  }
}
module.exports = {
  View,
  Create,
  Delete,
  Update,
  FindOne,
  run,
};
