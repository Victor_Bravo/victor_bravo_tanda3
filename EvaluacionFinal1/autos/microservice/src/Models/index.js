const { sequelize } = require("../settings");
const { DataTypes } = require("sequelize");
const { name } = require("../../package.json");

const Model = sequelize.define(name, {
  brand: { type: DataTypes.STRING },
  model: { type: DataTypes.STRING },
  color: { type: DataTypes.STRING },
  plaque: { type: DataTypes.STRING },
  image: { type: DataTypes.STRING },

},{freezeTableName:true});

async function SyncDB() {
  try {
    await Model.sync({ logging: false });

    return { statusCode: 200, data: "ok" };
  } catch (error) {
    console.log("soy un error" + error);
    return { statusCode: 500, message: error.toString() };
  }
}

module.exports = { SyncDB, Model };
