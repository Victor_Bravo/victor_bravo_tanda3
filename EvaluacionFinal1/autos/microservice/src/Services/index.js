const Controllers = require("../Controllers");
const { InternalError } = require("../settings");
const apiProbelmas = require("api-problema");

async function Create({ brand, model, color, plaque, image }) {
  try {
    let { statusCode, data, message } = await Controllers.Create({
      brand, 
      model, 
      color, 
      plaque,
      image, 
    });

    return { statusCode, data, message };

  } catch (error) {
    
    console.log({ step: "service Create", error: error.toString() });

    return { statusCode: 500, message: error.toString() };
  }
}

async function Delete({ id }) {
  try {
    const findOne= await Controllers.FindOne({
      where: { id },
    });

    if (findOne.statusCode !== 200) {

      switch (findOne.statusCode) {
        case 400:return {statusCode:400, message:"No existe el usuario a eliminar"};

        default:return {statusCode:500, message: InternalError};
      }

    }

    const del= await Controllers.Delete({where: { id }});

    if(del.statusCode === 200) return { statusCode:200, data: findOne.data};

    return {statusCode:400, message: InternalError}
  } catch (error) {
    console.log({ step: "service Delete", error: error.toString() });

    return { statusCode: 500, message: error.toString() };
  }
}

async function Update({ id, brand, model, color, plaque, image }) {
  try {
    let { statusCode, data, message } = await Controllers.Update({
      id,
      brand, 
      model, 
      color, 
      plaque,
      image, 
    });

    return { statusCode, data, message };

  } catch (error) {
    
    console.log({ step: "service Update", error: error.toString() });

    return { statusCode: 500, message: error.toString() };
  }
}
async function FindOne({ id }) {
  try {
    let { statusCode, data, message } = await Controllers.FindOne({
      where: { id },
    });
    
    return { statusCode, data, message };
  
  } catch (error) {
  
    console.log({ step: "service FindOne", error: error.toString() });

    return { statusCode: 500, message: error.toString() };
  }
}

async function View({}) {
  try {     
  
    let { statusCode, data, message } = await Controllers.View({});


    dato=[];
    for(let x of data){
     const {data}  = await apiProbelmas.FindOne({auto:x.id});
     x.idc=x.id;
     delete x.id;
     var union = Object.assign({},x, {...data})
     dato.push(union);
    }
    console.log("dato");
    todo=[...data,dato]
    return { statusCode, data:dato, message };
  
  } catch (error) {
  
    console.log({ step: "service View", error: error.toString() });

    return { statusCode: 500, message: error.toString() };
  }
}

module.exports = { Create, Delete, Update, FindOne, View };
//! el servicio puede combinar diferentes controladores
//? 500 significa error interno
