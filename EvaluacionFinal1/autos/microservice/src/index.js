const { SyncDB } = require("./Models");
const { run } = require("./Adapters/processor.js");

module.exports = { SyncDB, run };
