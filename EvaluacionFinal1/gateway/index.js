const express = require('express');
const http = require('http');

const app = express();
const server = http.createServer(app);
const { Server } = require('socket.io');
const apiAutos = require('api-autos');
const apiProblemas = require("api-problema");
const io = new Server(server);


server.listen(3001, ()=> {
    
    console.log("Server Initialize");
    
    io.on('connection', socket =>{
    
    console.log("new conection", socket.id);

    //autos

    socket.on('req:autos:view', async ({ })=>{
        try {
console.log("view");

            const {statusCode, data, message} = await apiAutos.View({});

            return io.to(socket.id).emit('res:autos:view',{statusCode, data, message})

        } catch (error) {

            console.log(error);
        }
        });

    socket.on('req:autos:create', async ({ brand, model, color, plaque, image })=>{
        try{
        console.log('req:autos:create', ({ brand, model, color, plaque, image}));

        const {statusCode, data, message} = await apiAutos.Create({brand, model, color, 
        plaque, image});
        
        io.to(socket.id).emit('res:autos:create',{statusCode, data, message});
        
        const view = await apiAutos.View({});

        return io.to(socket.id).emit('res:autos:view',view)
    
    } catch (error) {
    
        console.log(error);
    }
    });

    socket.on('req:autos:delete', async ({ id })=>{
        try{

        console.log('req:autos:delete', ({ id }));

        const {statusCode, data, message} = await apiAutos.Delete({id});

        io.to(socket.id).emit('res:autos:delete',{statusCode, data, message})

        const view = await apiAutos.View({});

        return io.to(socket.id).emit('res:autos:view',view);
    } catch (error) {
        console.log(error);
    }
    })

    socket.on('req:autos:update', async ({ id, brand, model, color, plaque, image })=>{
        
        try{
    
        console.log('req:autos:update', ({ id, brand, model, color, plaque, image }));

        const {statusCode, data, message} = await apiAutos.Update({ id, brand, model, color, plaque, image });
    
        return io.to(socket.id).emit('res:autos:update',{statusCode, data, message})
    
    } catch (error) {
        
        console.log(error);
    
    }

    });

    socket.on('req:autos:findOne', async ({ auto })=>{
        try{
        
        console.log('req:autos:findOne', ({ auto }));
    
        const {statusCode, data, message} = await apiAutos.FindOne({ auto });
    
        return io.to(socket.id).emit('res:autos:findOne',{statusCode, data, message})
    
    } catch (error) {

        console.log(error);
    
    }
    });

    //problema

    socket.on('req:problema:view', async ({ })=>{
        try {

            console.log("res:microservice:view Problemas");

            const {statusCode, data, message} = await apiProblemas.View({});

            return io.to(socket.id).emit('res:problema:view',{statusCode, data, message})
        
        } catch (error) {

            console.log(error);
        
        }
        });

    socket.on('req:problema:create', async ({ level, auto, description })=>{
        
        try{
        
            console.log('req:problema:create', ({ level, auto, description }));
        
            const {statusCode, data, message} = await apiProblemas.Create({ level, auto, description });
        
            io.to(socket.id).emit('res:problema:create',{statusCode, data, message});
        
            const view = await apiAutos.View({});

            return io.to(socket.id).emit('res:autos:view',view);

        } catch (error) {

            console.log(error);

        }
    });

    socket.on('req:problema:delete', async ({ id })=>{
        try{

            console.log('req:problema:delete', ({ id }));

            const {statusCode, data, message} = await apiProblemas.Delete({id});

            io.to(socket.id).emit('res:problema:delete',{statusCode, data, message})

            const view = await apiAutos.View({});

            return io.to(socket.id).emit('res:autos:view',view);

        } catch (error) {

            console.log(error);

        }
    })

    socket.on('req:problema:findOne', async ({ id })=>{
        try{

            console.log('req:problema:findOne', ({ id }));

            const {statusCode, data, message} = await apiProblemas.FindOne({id});

            return io.to(socket.id).emit('res:problema:findOne',{statusCode, data, message})

        } catch (error) {

            console.log(error);

        }
    });

    socket.on('req:problema:update', async ({ id, auto, description, level })=>{
        
        try{
    
        console.log('req:problema:update', ({ id, auto, description, level }));

        const {statusCode, data, message} = await apiProblemas.Update({ id, auto, description, level });
    
        return io.to(socket.id).emit('res:problema:update',{statusCode, data, message})
    
    } catch (error) {
        
        console.log(error);
    
    }

    });

    })
})