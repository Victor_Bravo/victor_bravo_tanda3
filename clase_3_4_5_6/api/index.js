const bull = require("bull");
const redis = {
  host: "localhost",
  port: 6379,
};const opts = {
  redis: { host: redis.host, port: redis.port },
};
const queueCreate = bull("curso:create", opts);

const queueDelete = bull("curso:delete", opts);

const queueUpdate = bull("curso:update", opts);

const queueFindOne = bull("curso:findOne", opts);

const queueView = bull("curso:View", opts);

async function Create({ name, age, color }) {
  try {
    const job = await queueCreate.add({ name, age, color });

    const {statusCode, data, message} = await job.finished();

    return {statusCode, data, message}

    if (statusCode === 200) {

      console.log("Welcome",data.name);

}
 else console.error(message);
 
} catch (error) {

  console.log(error);

}
}
async function Delete({ id }) {

  try {

    const job = await queueDelete.add({ id });

    const {statusCode, data, message} = await job.finished();

    return {statusCode, data, message};

    if (statusCode === 200) {

      console.log("User Delete ",data.name);

    }

    else console.error(message);  
  } catch (error) {

      console.log(error);
  }
}

async function Update({ name, color, id, age }) {

  try {

    const job = await queueUpdate.add({ name, color, id, age });

    const {statusCode, data, message} = await job.finished();

    return {statusCode, data, message};


    // console.log({statusCode, data, message});

  } catch (error) {

    console.log(error);

  }
}

async function FindOne({ id }) {

  try {

    const job = await queueFindOne.add({ id });

    const {statusCode, data, message} = await job.finished();

    return {statusCode, data, message};

    if (statusCode === 200) {

      console.log("Find name is: ",data);

    }

    else console.error(message);

  } catch (error) {

    console.log(error);

  }

}

async function View({}) {

  try {

    const job = await queueView.add({});

    const {statusCode, data, message} = await job.finished();

    return {statusCode, data, message};

if (statusCode === 200) for(let x of data) console.log(x);

  } catch (error) {

    console.log(error);

  }

}

async function main() {

  //await Create({name:"Spiderman", age:23, color:"Verde"})

  //  await Delete({id: 5});
  // await Update({name:"Jorge", id:9})
  // await FindOne({id:9}); 
  await View({});

}

main()
module.exports = {Create, Delete, Update, FindOne, View}