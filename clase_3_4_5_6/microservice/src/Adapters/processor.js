const Services = require("../Services");
const { InternalError } = require("../settings");
const {
  queueCreate,
  queueView,
  queueDelete,
  queueUpdate,
  queueFindOne,
} = require("./index");

async function View(job, done) {
  const {} = job.data;
  console.log(job.id);
  try {
    let { statusCode, data, message } = await Services.View({});

    done(null, { statusCode, data:data.map(x =>({...x.toJSON(), ...{worker: job.id}})), message });
  } catch (error) {
    console.log({ step: "service Adaptador", error: error.toString() });

    done(null, { statusCode: 500, message: InternalError });
  }
}
async function Create(job, done) {
  const { name, age, color } = job.data;
  try {
    let { statusCode, data, message } = await Services.Create({
      name,
      age,
      color,
    });

    done(null, { statusCode, data, message });
  } catch (error) {
    console.log({ step: "Adaptador queueCreate", error: error.toString() });

    done(null, { statusCode: 500, message: InternalError });
  }
}
async function Delete(job, done) {
  const { id } = job.data;
  try {
    let { statusCode, data, message } = await Services.Delete({ id });

    done(null, { statusCode, data, message });
  } catch (error) {
    console.log({ step: "Adaptador queueDelete", error: error.toString() });

    done(null, { statusCode: 500, message: InternalError });
  }
}
async function Update(job, done) {
  const { name, color, id, age } = job.data;
  try {
    let { statusCode, data, message } = await Services.Update({
      name,
      color,
      id,
      age,
    });

    done(null, { statusCode, data, message });
  } catch (error) {
    console.log({ step: "Adaptador Update", error: error.toString() });

    done(null, { statusCode: 500, message: InternalError });
  }
}
async function FindOne(job, done) {
  const { id } = job.data;
  try {
    let { statusCode, data, message } = await Services.FindOne({ id });

    done(null, { statusCode, data, message });
  } catch (error) {
    console.log({ step: "service Adaptador", error: error.toString() });

    done(null, { statusCode: 500, message: InternalError });
  }
}

async function run() {
  try {
    console.log("Vamos a inicializar worker");
    queueView.process(View);
    queueCreate.process(Create);
    queueDelete.process(Delete);
    queueUpdate.process(Update);
    queueFindOne.process(FindOne);
  } catch (error) {
    console.log("esto es un error" + error);
  }
}
module.exports = {
  View,
  Create,
  Delete,
  Update,
  FindOne,
  run,
};
