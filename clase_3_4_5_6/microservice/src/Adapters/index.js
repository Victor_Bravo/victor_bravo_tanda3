const bull = require("bull");
const { redis } = require("../settings");
const opts = {
  redis: { host: redis.host, port: redis.port },
};
const queueCreate = bull("curso:create", opts);

const queueDelete = bull("curso:delete", opts);

const queueUpdate = bull("curso:update", opts);

const queueFindOne = bull("curso:findOne", opts);

const queueView = bull("curso:View", opts);

module.exports = {
  queueCreate,
  queueDelete,
  queueUpdate,
  queueFindOne,
  queueView,
};
//*Cuando Inicie el Micro servicio Esto se ejecuta
//* node estara escuchando gracias a libreria bull
//* sudo docker-compose ud -d lo lenvanta en segundo plano
