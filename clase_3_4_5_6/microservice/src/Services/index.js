const Controllers = require("../Controllers");
const { InternalError } = require("../settings");
async function Create({ name, color, age }) {
  try {
    let { statusCode, data, message } = await Controllers.Create({
      name,
      color,
      age,
    });
    return { statusCode, data, message };
  } catch (error) {
    console.log({ step: "service Create", error: error.toString() });

    return { statusCode: 500, message: error.toString() };
  }
}
async function Delete({ id }) {
  try {
    const findOne= await Controllers.FindOne({
      where: { id },
    });

    if (findOne.statusCode !== 200) {
      
      // let response ={
      //   400:{statusCode:400, message:"No existe el usuario a eliminar"},
      //   500:{statusCode:500, message: InternalError}
      // }       return response[findOne.statusCode];


      switch (findOne.statusCode) {
        case 400:return {statusCode:400, message:"No existe el usuario a eliminar"};

        case 500:return {statusCode:500, message: InternalError};

        default:return {statusCode:findOne.statusCode, message:findOne.message}
      }

    }

    const del= await Controllers.Delete({where: { id }});
    if(del.statusCode === 200) return { statusCode:200, data: findOne.date};

    return {statusCode:400, message: InternalError}
  } catch (error) {
    console.log({ step: "service Delete", error: error.toString() });

    return { statusCode: 500, message: error.toString() };
  }
}

async function Update({ name, color, age, id }) {
  try {
    let { statusCode, data, message } = await Controllers.Update({
      name,
      color,
      age,
      id,
    });
    return { statusCode, data, message };
  } catch (error) {
    console.log({ step: "service Update", error: error.toString() });

    return { statusCode: 500, message: error.toString() };
  }
}
async function FindOne({ id }) {
  try {
    let { statusCode, data, message } = await Controllers.FindOne({
      where: { id },
    });
    return { statusCode, data, message };
  } catch (error) {
    console.log({ step: "service FindOne", error: error.toString() });

    return { statusCode: 500, message: error.toString() };
  }
}
async function View({}) {
  try {
    let { statusCode, data, message } = await Controllers.View({});
    return { statusCode, data, message };
  } catch (error) {
    console.log({ step: "service View", error: error.toString() });

    return { statusCode: 500, message: error.toString() };
  }
}
module.exports = { Create, Delete, Update, FindOne, View };
//! el servicio puede combinar diferentes controladores
//? 500 significa error interno
