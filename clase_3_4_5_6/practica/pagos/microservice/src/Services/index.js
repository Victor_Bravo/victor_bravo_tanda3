const Controllers = require("../Controllers");
const { InternalError, redis } = require("../settings");
const socios = require("api-socios");

async function Create({ socio, amount }) {
  try {

    const validadSocio = await socios.FindOne({id:socio}, redis);

    if (validadSocio.statusCode !== 200) {
      
      switch (validadSocio.statusCode) {
        case 400:return{statusCode:400, message:"No Existe el Usuario"}
      
        default: return {statusCode:500, message:InternalError}
                }
    }

    if(!validadSocio.data.enable){ return{statusCode:200, message:"El socio no esta habilitado"}
  }
    let { statusCode, data, message } = await Controllers.Create({ socio, amount });

    return { statusCode, data, message };
  } catch (error) {
    console.log({ step: "service Create", error: error.toString() });

    return { statusCode: 500, message: error.toString() };
  }
}
async function Delete({ id }) {
  try {
    const findOne= await Controllers.FindOne({
      where: { id },
    });

    if (findOne.statusCode !== 200) {

      switch (findOne.statusCode) {

        case 400:return {statusCode:400, message:"No existe el usuario a eliminar"};

        default:return {statusCode:500, message: InternalError};

      }

    }

    const del= await Controllers.Delete({where: { id }});
    if(del.statusCode === 200) return { statusCode:200, data: findOne.date};

    return {statusCode:400, message: InternalError}
  } catch (error) {
    console.log({ step: "service Delete", error: error.toString() });

    return { statusCode: 500, message: error.toString() };
  }
}

async function FindOne({ id }) {
  try {
    let { statusCode, data, message } = await Controllers.FindOne({
      where: { id },
    });
    return { statusCode, data, message };
  } catch (error) {
    console.log({ step: "service FindOne", error: error.toString() });

    return { statusCode: 500, message: error.toString() };
  }
}
async function View({}) {
  try {
    let { statusCode, data, message } = await Controllers.View({});
    return { statusCode, data, message };
  } catch (error) {
    console.log({ step: "service View", error: error.toString() });

    return { statusCode: 500, message: error.toString() };
  }
}
module.exports = { Create, Delete, FindOne, View };
//! el servicio puede combinar diferentes controladores
//? 500 significa error interno
