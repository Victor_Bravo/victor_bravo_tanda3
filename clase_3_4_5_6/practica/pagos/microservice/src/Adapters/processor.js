const Services = require("../Services");
const { InternalError } = require("../settings");
const {
  queueCreate,
  queueView,
  queueDelete,
  queueUpdate,
  queueFindOne,
} = require("./index");

async function View(job, done) {
  const {} = job.data;
  try {
    let { statusCode, data, message } = await Services.View({});

    done(null, { statusCode, data:data, message });
  } catch (error) {
    console.log({ step: "service Adaptador", error: error.toString() });

    done(null, { statusCode: 500, message: InternalError });
  }
}
async function Create(job, done) {
  const { socio, amount } = job.data;
  try {
    let { statusCode, data, message } = await Services.Create({ socio, amount });

    done(null, { statusCode, data, message });
  } catch (error) {
    console.log({ step: "Adaptador queueCreate", error: error.toString() });

    done(null, { statusCode: 500, message: InternalError });
  }
}
async function Delete(job, done) {
  const { id } = job.data;
  try {
    let { statusCode, data, message } = await Services.Delete({ id });

    done(null, { statusCode, data, message });
  } catch (error) {
    console.log({ step: "Adaptador queueDelete", error: error.toString() });

    done(null, { statusCode: 500, message: InternalError });
  }
}

async function FindOne(job, done) {
  const { id } = job.data;
  try {
    let { statusCode, data, message } = await Services.FindOne({ id });

    done(null, { statusCode, data, message });
  } catch (error) {
    console.log({ step: "service Adaptador", error: error.toString() });

    done(null, { statusCode: 500, message: InternalError });
  }
}

async function run() {
  try {
    console.log("Vamos a inicializar worker");
    queueView.process(View);
    queueCreate.process(Create);
    queueDelete.process(Delete);
    queueFindOne.process(FindOne);
  } catch (error) {
    console.log("esto es un error" + error);
  }
}
module.exports = {
  View,
  Create,
  Delete,
  FindOne,
  run,
};
