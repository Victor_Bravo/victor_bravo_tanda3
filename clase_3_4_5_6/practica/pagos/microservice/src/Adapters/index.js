const bull = require("bull");

const {name} = require("../../package.json");

const { redis } = require("../settings");

const opts = {
  redis: { host: redis.host, port: redis.port },
};
 console.log(`${name}:create`);
const queueCreate = bull(`${name}:create`, opts);

const queueDelete = bull(`${name}:delete`, opts);

const queueFindOne = bull(`${name}:findOne`, opts);

const queueView = bull(`${name}:View`, opts);

module.exports = {
  queueCreate,
  queueDelete,
  queueFindOne,
  queueView,
};
//*Cuando Inicie el Micro servicio Esto se ejecuta
//* node estara escuchando gracias a libreria bull
//* sudo docker-compose ud -d lo lenvanta en segundo plano
