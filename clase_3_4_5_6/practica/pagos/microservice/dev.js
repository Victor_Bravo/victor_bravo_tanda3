const micro = require("./src");
const dotenv = require("dotenv");
dotenv.config();

async function main() {
  try {
  
    const db = await micro.SyncDB();
    if (db.statusCode !== 200) throw db.message;
    await micro.run();
  } catch (error) {
    console.log("esto es un error" + error);
  }
}
main();
