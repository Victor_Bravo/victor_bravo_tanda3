const bull = require("bull");
const { name } = require("./package.json")
const redis = {
  host: "localhost",
  port: 6379,
};const opts = {
  redis: { host: redis.host, port: redis.port },
};
const queueCreate = bull(`${name.replace('-api', '')}:create`, opts);

const queueDelete = bull(`${name.replace('-api', '')}:delete`, opts);

const queueUpdate = bull(`${name.replace('-api', '')}:update`, opts);

const queueFindOne = bull(`${name.replace('-api', '')}:findOne`, opts);

const queueView = bull(`${name.replace('-api', '')}:View`, opts);

const queueEnable = bull(`${name.replace('-api', '')}:enable`, opts);

const queueDisable = bull(`${name.replace('-api', '')}:disable`, opts);

async function Enable({ id }) {
  try {
    const job = await queueEnable.add({ id });

    const {statusCode, data, message} = await job.finished();

    return {statusCode, data, message}

    if (statusCode === 200) {

      console.log("Welcome",data.name);

}
 else console.error(message);
 
} catch (error) {

  console.log(error);

}
}

async function Disable({ id }) {
  try {
    const job = await queueDisable.add({ id });

    const {statusCode, data, message} = await job.finished();

    return {statusCode, data, message}

    if (statusCode === 200) {

      console.log("Welcome",data.name);

}
 else console.error(message);
 
} catch (error) {

  console.log(error);

}
}

async function Create({ phone, name }) {
  try {
    const job = await queueCreate.add({ phone, name });

    const {statusCode, data, message} = await job.finished();

    return {statusCode, data, message}

    if (statusCode === 200) {

      console.log("Welcome",data.name);

}
 else console.error(message);
 
} catch (error) {

  console.log(error);

}
}
async function Delete({ id }) {

  try {

    const job = await queueDelete.add({ id });

    const {statusCode, data, message} = await job.finished();

    return {statusCode, data, message};

    if (statusCode === 200) {

      console.log("User Delete ",data.name);

    }

    else console.error(message);  
  } catch (error) {

      console.log(error);
  }
}

async function Update({ id, name, phone, email, age }) {

  try {

    const job = await queueUpdate.add({ id, name, phone, email, age });

    const {statusCode, data, message} = await job.finished();

    return {statusCode, data, message};

    // console.log({statusCode, data, message});

  } catch (error) {

    console.log(error);

  }
}

async function FindOne({ id }) {

  try {

    const job = await queueFindOne.add({ id });

    const {statusCode, data, message} = await job.finished();

    return {statusCode, data, message};

    if (statusCode === 200) {

      console.log("Find name is: ",data);

    }

    else console.error(message);

  } catch (error) {

    console.log(error);

  }

}

async function View({ enable }) {

  try {

    const job = await queueView.add({ enable });

    const {statusCode, data, message} = await job.finished();

    return {statusCode, data, message};

if (statusCode === 200) for(let x of data) console.log(x);

  } catch (error) {

    console.log(error);

  }

}

async function main() {

  //await Create({name:"Spiderman", age:23, color:"Verde"})

  //  await Delete({id: 5});
  // await Update({name:"Jorge", id:9})
  // await FindOne({id:9}); 
  await View({});

}

main()
module.exports = {Create, Delete, Update, FindOne, View, Enable, Disable}