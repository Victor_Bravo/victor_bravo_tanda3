const Services = require("../Services");
const { InternalError } = require("../settings");
const {
  queueCreate,
  queueView,
  queueDelete,
  queueUpdate,
  queueFindOne,
  queueEnable,
  queueDisable,
} = require("./index");

async function View(job, done) {
  const { enable } = job.data;
  try {
    let { statusCode, data, message } = await Services.View({ enable });

    done(null, { statusCode, data:data, message });
  } catch (error) {
    console.log({ step: "service Adaptador", error: error.toString() });

    done(null, { statusCode: 500, message: InternalError });
  }
}
async function Create(job, done) {
 
  const { name, phone } = job.data;
 
  try {
    let { statusCode, data, message } = await Services.Create({
      name,
      phone,
    });

    done(null, { statusCode, data, message });
  } catch (error) {
    console.log({ step: "Adaptador queueCreate", error: error.toString() });

    done(null, { statusCode: 500, message: InternalError });
  }
}
async function Delete(job, done) {
  const { id } = job.data;
  try {
    let { statusCode, data, message } = await Services.Delete({ id });

    done(null, { statusCode, data, message });
  } catch (error) {
    console.log({ step: "Adaptador queueDelete", error: error.toString() });

    done(null, { statusCode: 500, message: InternalError });
  }
}
async function Update(job, done) {
  const { id, name, phone, email, age } = job.data;
  try {
    let { statusCode, data, message } = await Services.Update({
      id, name, phone, email, age
    });

    done(null, { statusCode, data, message });
  } catch (error) {
    console.log({ step: "Adaptador Update", error: error.toString() });

    done(null, { statusCode: 500, message: InternalError });
  }
}
async function FindOne(job, done) {
  const { id } = job.data;
  try {
    let { statusCode, data, message } = await Services.FindOne({ id });

    done(null, { statusCode, data, message });
  } catch (error) {
    console.log({ step: "service Adaptador", error: error.toString() });

    done(null, { statusCode: 500, message: InternalError });
  }
}

async function Enable(job, done) {
  const { id } = job.data;
  try {
    let { statusCode, data, message } = await Services.Enable({ id });

    done(null, { statusCode, data, message });
  } catch (error) {
    console.log({ step: "Adaptador Enable", error: error.toString() });

    done(null, { statusCode: 500, message: InternalError });
  }
}

async function Disable(job, done) {
  const { id } = job.data;
  try {
    let { statusCode, data, message } = await Services.Disable({ id });

    done(null, { statusCode, data, message });
  } catch (error) {
    console.log({ step: "Adaptador Disable", error: error.toString() });

    done(null, { statusCode: 500, message: InternalError });
  }
}

async function run() {
  try {
    console.log("Vamos a inicializar worker");
    queueView.process(View);

    queueCreate.process(Create);
    
    queueDelete.process(Delete);
    
    queueUpdate.process(Update);
    
    queueFindOne.process(FindOne);
    
    queueEnable.process(Enable);
    
    queueDisable.process(Disable);

  } catch (error) {
    console.log("esto es un error" + error);
  }
}
module.exports = {
  View,
  Create,
  Delete,
  Update,
  FindOne,
  run,
};
