const bull = require("bull");
const {name} = require("./package.json");

const redis = {
  host: "localhost",
  port: 6379,
};const opts = {
  redis: { host: redis.host, port: redis.port },
};
const queueCreate = bull(`${name.replace('-api', '')}:create`, opts);

const queueDelete = bull(`${name.replace('-api', '')}:delete`, opts);

const queueUpdate = bull(`${name.replace('-api', '')}:update`, opts);

const queueFindOne = bull(`${name.replace('-api', '')}:findOne`, opts);

const queueView = bull(`${name.replace('-api', '')}:View`, opts);

async function Create({ title, image }) {
  try {
    const job = await queueCreate.add({ title, image });

    const {statusCode, data, message} = await job.finished();

    return {statusCode, data, message}

    if (statusCode === 200) {

      console.log("Welcome",data);

}
 else console.error(message);
 
} catch (error) {

  console.log(error);

}
}
async function Delete({ id }) {

  try {

    const job = await queueDelete.add({ id });

    const {statusCode, data, message} = await job.finished();

    return {statusCode, data, message};

    if (statusCode === 200) {

      console.log("User Delete ",data.name);

    }

    else console.error(message);  
  } catch (error) {

      console.log(error);
  }
}

async function Update({ title, category, id, seccions }) {

  try {

    const job = await queueUpdate.add({  title, category, id, seccions  });

    const {statusCode, data, message} = await job.finished();

    return {statusCode, data, message};

    // console.log({statusCode, data, message});

  } catch (error) {

    console.log(error);

  }
}

async function FindOne({ title }) {

  try {

    const job = await queueFindOne.add({ title });

    const {statusCode, data, message} = await job.finished();

    return {statusCode, data, message};

    if (statusCode === 200) {

      console.log("Find name is: ",data);

    }

    else console.error(message);

  } catch (error) {

    console.log(error);

  }

}

async function View({}) {

  try {

    const job = await queueView.add({});

    const {statusCode, data, message} = await job.finished();

    return {statusCode, data, message};

if (statusCode === 200) for(let x of data) console.log(x);

  } catch (error) {

    console.log(error);

  }

}

async function main() {

}

module.exports = {Create, Delete, Update, FindOne, View}