const api = require('./api');

async function main() {
    
    try {
      let {data, statusCode, message} = await api.Create({image:"https://20palabras.com/wp-content/uploads/2016/12/principito.jpg", title:"El Principito"});

      console.log({data, statusCode, message});
    } catch (error) {

        console.error(error);
        
    }
   

}

async function view() {
    
  try {
    let {data, statusCode, message} = await api.View({});

    console.log({data, statusCode, message});
  } catch (error) {

      console.error(error);
      
  }
 

}
async function dele() {
    
  try {
    let {data, statusCode, message} = await api.Delete({id:9});

    console.log({data, statusCode, message});
  } catch (error) {

      console.error(error);
      
  }

}
main();