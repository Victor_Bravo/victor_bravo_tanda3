const Services = require("../Services");
const { InternalError } = require("../settings");
const {
  queueCreate,
  queueView,
  queueDelete,
  queueUpdate,
  queueFindOne,
} = require("./index");

async function View(job, done) {
  const {} = job.data;
  try {
    let { statusCode, data, message } = await Services.View({});

    done(null, { statusCode, data, message });
  } catch (error) {
    console.log({ step: "service Adaptador", error: error.toString() });

    done(null, { statusCode: 500, message: InternalError });
  }
}
async function Create(job, done) {
  const { title, image } = job.data;
  try {
    let { statusCode, data, message } = await Services.Create({ title, image });

    done(null, { statusCode, data, message });
  } catch (error) {
    console.log({ step: "Adaptador queueCreate", error: error.toString() });

    done(null, { statusCode: 500, message: InternalError });
  }
}
async function Delete(job, done) {
  const { id } = job.data;
  try {
    let { statusCode, data, message } = await Services.Delete({ id });

    done(null, { statusCode, data, message });
  } catch (error) {
    console.log({ step: "Adaptador queueDelete", error: error.toString() });

    done(null, { statusCode: 500, message: InternalError });
  }
}
async function Update(job, done) {
  const { title, category, id, seccions } = job.data;
  try {
    let { statusCode, data, message } = await Services.Update({
      title, 
      category, 
      id, 
      seccions,
    });

    done(null, { statusCode, data, message });
  } catch (error) {
    console.log({ step: "Adaptador Update", error: error.toString() });

    done(null, { statusCode: 500, message: InternalError });
  }
}
async function FindOne(job, done) {
  const { title } = job.data;
  try {
    let { statusCode, data, message } = await Services.FindOne({ title });

    done(null, { statusCode, data, message });
  } catch (error) {
    console.log({ step: "service Adaptador", error: error.toString() });

    done(null, { statusCode: 500, message: InternalError });
  }
}

async function run() {
  try {
    // setTimeout( async() => {
    //   console.log("creando prueba");

    //   const job = await queueCreate.add({ image:"https://1.bp.blogspot.com/-KoFxJq7OwtQ/Uksd_2uY-EI/AAAAAAAAlzA/3wuuhGwsyTc/s640/thehungergams-catchingfire-ukposter.jpg", title:"Los Juegos Del Hambre" });

    //   const {statusCode, data, message} = await job.finished();    

    //   console.log({statusCode, data, message});
    // }, 2000);
    console.log("Vamos a inicializar worker");
    queueView.process(View);
    queueCreate.process(Create);
    queueDelete.process(Delete);
    queueUpdate.process(Update);
    queueFindOne.process(FindOne);
  } catch (error) {
    console.log("esto es un error" + error);
  }
}
module.exports = {
  View,
  Create,
  Delete,
  Update,
  FindOne,
  run,
};
