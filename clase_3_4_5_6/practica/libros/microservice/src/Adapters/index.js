const bull = require("bull");
const { redis } = require("../settings");
const { name } = require("../../package.json")
const opts = {
  redis: { host: redis.host, port: redis.port },
};
console.log(`${name}:create`);
const queueCreate = bull(`${name}:create`, opts);

const queueDelete = bull(`${name}:delete`, opts);

const queueUpdate = bull(`${name}:update`, opts);

const queueFindOne = bull(`${name}:findOne`, opts);

const queueView = bull(`${name}:View`, opts);

module.exports = {
  queueCreate,
  queueDelete,
  queueUpdate,
  queueFindOne,
  queueView,
};
//*Cuando Inicie el Micro servicio Esto se ejecuta
//* node estara escuchando gracias a libreria bull
//* sudo docker-compose ud -d lo lenvanta en segundo plano
