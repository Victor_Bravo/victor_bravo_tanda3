const { Model } = require("../Models");

async function Create({ title, image }) {
  try {
    let instance = await Model.create(
      { title, image }, { logging:false }
    );
    return { statusCode: 200, data: instance.toJSON() };
  } catch (error) {
    console.log({ step: "Controllers Create", error: error.toString() });
    return { statusCode: 400, message: error.toString() };
  }
}

async function Delete({where = {}}) {
  try {
    await Model.destroy({ where, logging:false });
    return { statusCode: 200, date: "ok" };
  } catch (error) {
    console.log({ step: "Controllers Delete", error: error.toString() });
    return { statusCode: 400, message: error.toString() };
  }
}

async function Update({ title, category, id, seccions }) {
  try {
    let instance = await Model.update({ title, category, seccions}, { where: { id }, logging:false, returning:true });
    return { statusCode: 200, data: instance[1][0].toJSON() };
  } catch (error) {
    console.log({ step: "Controllers Update", error: error.toString() });
    return { statusCode: 400, message: error.toString() };
  }
}

async function FindOne({ where = {title} }) {
  try {
    let instance = await Model.findOne({ where });

    if(instance) return { statusCode: 200, data: instance.toJSON() };

    else return {statusCode:400, message:"No Existe El Usuario"}
  } catch (error) {
    console.log({ step: "Controllers FindOne", error: error.toString() });
    return { statusCode: 500, message: error.toString() };
  }
}

async function View({ where = {} }) {
  try {
    let instance = await Model.findAll({ where, logging:false });
    return { statusCode: 200, data: instance };
  } catch (error) {
    console.log({ step: "Controllers View", error: error.toString() });
    return { statusCode: 500, message: error.toString() };
  }
}

module.exports = { Create, Delete, Update, FindOne, View };
