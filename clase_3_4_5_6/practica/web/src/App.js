import React from "react";
import styled from "styled-components";
import Socios from "./Socios"
import Libros from "./Libros";
import Pagos from "./Pagos";

const Container = styled.div`
display: flex;
flex-direction: row;	
min-height:calc(100vh);
`;

const Slider = styled.div`
	width: 35%;
	min-width: 300px;
`;

const Body = styled.div`
	width: 65%;
	min-width: 350px;

`

function App() {
	

  	return (
  <Container>
	<Slider>			
	<Socios />
	<Pagos />
	</Slider>
	<Body>
		<Libros />
	</Body>
  </Container>  	
  	);
}

export default App;
