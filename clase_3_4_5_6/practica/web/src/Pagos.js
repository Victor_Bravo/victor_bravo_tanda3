import React, { useEffect, useState} from "react";
import styled from "styled-components";

import { socket } from "./ws";

const Container = styled.div`
    margin-top: 30px;
    width: 75%;
    max-width: 100%;
    height: calc(50vh);
    overflow: scroll;

`;

const Socio = styled.div`
    margin-top: 15px;
    display: flex;
    flex-direction: column;
    background-color:#303536;
    border-radius:15px;
    color: white;
    padding-bottom: 10px;
    

`;

const Body = styled.div`
    padding-left: 15px;
    padding-right: 15px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
`;


const Button = styled.button`
    background-color: #e71414a1;
    padding: 10px 20px 10px 20px;
    border-radius: 15px;
    opacity: 0.8;
    font-weight: bold;
    font-size: 1rem;
    color: white;
    top: 20px;
    left: 20px;
    position: relative;
    margin-bottom: 10px;

`;

const Icon = styled.img`
    cursor: pointer;
    width: 35px;
    height: 35px;
`;  
const Name = styled.p`
`;

const Input = styled.input`
margin-top: 40px;
margin-left: 10px;
display: block;
width: 68%;
height: 25px;
cursor: pointer;
`;
const App = () =>{
    const [data, setData] = useState([]);
    const [value, setValue] = useState([]);

	useEffect(()=>{

		socket.on('res:pagos:view',({ statusCode, data, message })=>{

			console.log('res:pagos:view',{ statusCode, data, message });
			
			console.log({statusCode, data, message});
			
			if(statusCode === 200) setData(data);
			
		});
        setTimeout(() => socket.emit('req:pagos:view',({ })), 1000);


	},[])		
    const handleCreate = () => value > 0 ? socket.emit('req:pagos:create',{socio:value, amount:300} ): null;
    
    const handleDelete = (id) =>{
        socket.emit('req:pagos:delete',{id})
    }

    const handleInput = (e) =>{
       setValue(e.target.value);
    }

    return(
        <>  
        <Button  onClick={handleCreate}>Create</Button> 
        <Input type={'number'} onChange={handleInput} ></Input>
          

        <Container>
			{
            
            data.map((x, i) => (
            <Socio>

                <Body>
                <Icon src="https://cdn-icons-png.flaticon.com/512/458/458594.png" onClick= {()=>handleDelete(x.id)} />
                    
                    <Name>socio:{x.socio}</Name>
                </Body> 
                <Name> cupon dr pago: {x.id}</Name>
                <Name>{x.createdAt}</Name>
                <Body>
                    
                </Body> 
            </Socio>           
                ))    
                }
			
        </Container>
        </>
    )

}

export default App;