import React, { useEffect, useState } from "react";
import {socket} from "./ws";
import styled from "styled-components";

const Container = styled.div`

    position: relative;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-evenly;
    align-content: stretch;
    display: flex;
    width: 100%;
    gap: ${props => props.gap};
    margin: ${props => props.margin};
`;

const Libro = styled.div`
    background-color:#303536 ;
    margin-bottom: 10px;
    border-radius:10px;
    padding: 2px;
    display: flex;
    flex-direction: column;
    position: relative;
    margin-top: 80px;

`;

const Portada = styled.img`
    width: 200px;
    height: 304px;
    border-top-left-radius: 10px;
    border-top-right-radius: 10px;
`;

const Icon = styled.img`
    cursor: pointer;
    position: absolute;
    opacity: calc(65%);
    top: 0;
    margin: 5px;
    right: 0;
    width: 35px;
    height: 35px;
`;

const Button = styled.button`
    background-color: #036e00;
    padding: 10px 20px 10px 20px;
    border-radius: 15px;
    opacity: 0.8;
    font-weight: bold;
    font-size: 1rem;
    color: white;
    position: absolute;
    top: 20px;
    right: 20px;

`

const Title = styled.p`
	color: white;
    text-align: center;
`
const Item = ({image, title, id}) =>{
    
    const handleDelete = () =>socket.emit('req:libros:delete',{id});

  


    const opts = {
        src:"https://cdn-icons-png.flaticon.com/512/458/458594.png",
        onClick: handleDelete
    };

    return(
                    <Libro>
                        <Icon {...opts} />
                        <Portada src={image} />
                        <Title>
                            {title}
                        </Title>
                    </Libro>
    )
}


const App= () => {

    const [data, setData] = useState([]);

	useEffect(()=>{

		socket.on('res:libros:view',({ statusCode, data, message })=>{

			console.log('res:libros:view',{ statusCode, data, message });
			
			console.log({statusCode, data, message});
			
			if(statusCode === 200) setData(data);
			
		});

        setTimeout(() => socket.emit('req:libros:view',({})), 1000);

	},[])

    const handleCreate = () =>{
        socket.emit('req:libros:create',{image:"https://1.bp.blogspot.com/-KoFxJq7OwtQ/Uksd_2uY-EI/AAAAAAAAlzA/3wuuhGwsyTc/s640/thehungergams-catchingfire-ukposter.jpg", title:"Los Juegos Del Hambre"})
    }

    return(

        <Container>
            
            <Button  onClick={handleCreate}>Create</Button>
            {

                data.map((x,i)=>( <Item {...x}  />))

            }
        </Container>
    )

}

export default App;